import { BasicColumn, FormSchema } from '/@/components/Table';

export const columns: BasicColumn[] = [
  {
    title: '接口名称',
    dataIndex: 'apiName',
    width: 200,
  },
  {
    title: '接口地址',
    dataIndex: 'url',
    width: 180,
  },
  {
    title: '请求方法',
    dataIndex: 'method',
    width: 50,
  },
  {
    title: '分组',
    dataIndex: 'group',
    width: 50,
  },

  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  }
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'apiName',
    label: '接口名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'url',
    label: '接口地址',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'group',
    label: '分组',
    component: 'Input',
    colProps: { span: 6 },
  }
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: '编号',
    component: 'InputNumber',
    show: false,
  },
  {
    field: 'apiName',
    label: '接口名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'url',
    label: '接口地址',
    required: true,
    component: 'Input',
  },
  {
    label: '请求方法',
    field: 'method',
    required: true,
    component: 'Input',
  },
  {
    label: '分组',
    field: 'group',
    required: true,
    component: 'Input',
  }
];
