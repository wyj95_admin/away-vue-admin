import { BasicColumn, FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import Icon from '@/components/Icon/Icon.vue';
import { RouteMeta } from 'vue-router';

function toRouteMeta(record: Recordable<any>) {
  return JSON.parse(record['meta']) as RouteMeta
}

function changeMeta<T>(formModel: Recordable<any>, key: string, val: T) {
  const meta = toRouteMeta(formModel);
  meta[key] = val;
  formModel.meta = JSON.stringify(meta);
}

export const columns: BasicColumn[] = [
  {
    title: '菜单名称',
    dataIndex: 'name',
    width: 200,
    align: 'left',
    customRender: ({ record }) => {
      const meta = toRouteMeta(record);
      return meta.title;
    },
  },
  {
    title: '图标',
    dataIndex: 'icon',
    width: 50,
    customRender: ({ record }) => {
      const meta = toRouteMeta(record);
      return h(Icon, { icon: meta.icon });
    },
  },
  {
    title: '组件',
    dataIndex: 'component',
  },
  {
    title: '排序',
    dataIndex: 'orderNo',
    width: 50,
    customRender: ({ record }) => {
      const meta = toRouteMeta(record);
      return meta.orderNo;
    },
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 0;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
];


export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '菜单名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '停用', value: 1 },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: '编号',
    component: 'InputNumber',
    show: false,
  },
  {
    field: 'name',
    label: '路由名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'parentId',
    label: '上级菜单',
    component: 'TreeSelect',
    componentProps: {
      fieldNames: {
        label: 'title',
        key: 'id',
        value: 'id',
      },
      getPopupContainer: () => document.body,
    },
  },
  {
    field: 'alias',
    label: '别名',
    component: 'Input'
  },
  {
    field: 'redirect',
    label: '跳转地址',
    component: 'Input'
  },
  {
    field: 'path',
    label: '路由地址',
    component: 'Input',
    required: true
  },
  {
    field: 'component',
    label: '组件路径',
    component: 'Input'
  },
  {
    field: 'meta',
    label: '路由参数',
    component: 'InputTextArea',
    defaultValue: JSON.stringify({}),
    show: false,
    componentProps: ({ formModel }) => {
      return {
        onChange: () => {
          const meta = toRouteMeta(formModel);
          const metaKeys = Object.keys(meta);
          for (let key of metaKeys) {
            formModel[key] = meta[key];
          }
        },
      };
    }
  },
  {
    field: 'title',
    label: '菜单名称',
    component: 'Input',
    required: true,
    componentProps: ({ formModel }) => {
      return {
        onkeyup: (event) => changeMeta(formModel, 'title', event.target?._value as string)
      };
    }
  },
  {
    field: 'icon',
    label: '图标',
    component: 'IconPicker',
    componentProps: ({ formModel }) => {
      return {
        onChange: (val: string) => changeMeta(formModel, 'icon', val)
      }
    }
  },
  {
    field: 'orderNo',
    label: '排序',
    component: 'InputNumber',
    show: false,
    componentProps: ({ formModel }) => {
      return {
        onChange: (val: number) => changeMeta(formModel, 'orderNo', val)
      }
    }
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 1 },
      ],
    }
  },
  {
    field: 'hideTab',
    label: '标签页隐藏',
    component: 'RadioButtonGroup',
    defaultValue: false,
    componentProps: ({ formModel }) => {
      return {
        options: [
          { label: '否', value: false },
          { label: '是', value: true }
        ],
        onChange: (val: boolean) => changeMeta(formModel, 'hideTab', val)
      }
    }
  },
  {
    field: 'affix',
    label: '固定标签',
    component: 'RadioButtonGroup',
    defaultValue: false,
    componentProps: ({ formModel }) => {
      return {
        options: [
          { label: '否', value: false },
          { label: '是', value: true }
        ],
        onChange: (val: boolean) => changeMeta(formModel, 'affix', val)
      }
    }
  },
  {
    field: 'ignoreKeepAlive',
    label: '是否缓存',
    component: 'RadioButtonGroup',
    defaultValue: false,
    componentProps: ({ formModel }) => {
      return {
        options: [
          { label: '否', value: false },
          { label: '是', value: true }
        ],
        onChange: (val: boolean) => changeMeta(formModel, 'ignoreKeepAlive', val)
      }
    }
  },
  {
    field: 'ignoreRoute',
    label: '在菜单隐藏',
    component: 'RadioButtonGroup',
    defaultValue: false,
    componentProps: ({ formModel }) => {
      return {
        options: [
          { label: '否', value: false },
          { label: '是', value: true }
        ],
        onChange: (val: boolean) => changeMeta(formModel, 'ignoreRoute', val)
      }
    }
  },
  {
    field: 'hideBreadcrumb',
    label: '在面包屑隐藏',
    component: 'RadioButtonGroup',
    defaultValue: false,
    componentProps: ({ formModel }) => {
      return {
        options: [
          { label: '否', value: false },
          { label: '是', value: true }
        ],
        onChange: (val: boolean) => changeMeta(formModel, 'hideBreadcrumb', val)
      }
    }
  },
];
