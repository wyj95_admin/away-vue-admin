import { h } from 'vue';
import { BasicColumn, FormSchema } from '/@/components/Table';
import { CodeEditor, MODE } from '/@/components/CodeEditor';


export const columns: BasicColumn[] = [
  {
    title: '说明',
    dataIndex: 'description',
    width: 100
  },
  {
    title: '工作名称',
    dataIndex: 'jobName',
    width: 100,
  },
  // {
  //   title: '工作分组',
  //   dataIndex: 'jobGroup',
  //   width: 100,
  // },
  {
    title: '类名称',
    dataIndex: 'jobClassName',
    width: 100
  }
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'jobName',
    label: '工作名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'jobClassName',
    label: '类名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'description',
    label: '说明',
    component: 'Input',
    colProps: { span: 6 },
  }
];


export const formSchema: FormSchema[] = [
  {
    field: 'requestsRecovery',
    label: 'requestsRecovery',
    component: 'RadioButtonGroup',
    show: false,
    componentProps: {
      options: [
        { label: '否', value: false },
        { label: '是', value: true }
      ]
    }
  },
  {
    field: 'isNonconcurrent',
    label: 'isNonconcurrent',
    component: 'RadioButtonGroup',
    show: false,
    componentProps: {
      options: [
        { label: '否', value: false },
        { label: '是', value: true }
      ]
    }
  },
  {
    field: 'isDurable',
    label: 'isDurable',
    component: 'RadioButtonGroup',
    show: false,
    componentProps: {
      options: [
        { label: '否', value: false },
        { label: '是', value: true }
      ]
    }
  },
  {
    field: 'isUpdateData',
    label: 'isUpdateData',
    component: 'RadioButtonGroup',
    show: false,
    componentProps: {
      options: [
        { label: '否', value: false },
        { label: '是', value: true }
      ]
    }
  },
  {
    field: 'schedName',
    label: '调度器',
    component: 'Input',
    show: false,
  },
  {
    field: 'jobName',
    label: '工作名称',
    component: 'Input',
    show: true,
    required: true
  },
  {
    field: 'jobGroup',
    label: '工作分组',
    required: true,
    component: 'Input',
  },
  {
    label: '说明',
    field: 'description',
    component: 'InputTextArea',
    defaultValue: ''
  },
  {
    field: 'jobClassName',
    label: '类名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'jobData',
    label: '工作参数',
    component: 'InputTextArea',
    render: ({ model }) => {
      return h(CodeEditor, {
        value: model.jobData,
        mode: MODE.JSON,
        onChange: (val) => {
          if (val == "") {
            return
          }
          model.jobData = val
        }
      });
    }
  }
];
