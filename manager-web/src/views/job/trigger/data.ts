import { h } from 'vue';
import { BasicColumn, FormSchema } from '/@/components/Table';
import { Tag } from 'ant-design-vue';

export const columns: BasicColumn[] = [
  // {
  //   title: '计划名称',
  //   dataIndex: 'triggerName',
  //   width: 100,
  // },
  {
    title: '说明',
    dataIndex: 'description',
    width: 180,
  },
  {
    title: '状态',
    dataIndex: 'triggerState',
    width: 90,
    customRender: ({ record }) => {
      switch (record.triggerState) {
        case 'WAITING':
          return h(Tag, {
            color: 'default'
          }, () => '等待')
        case 'ACQUIRED':
          return h(Tag, {
            color: 'blue'
          }, () => '启动')
        case 'PAUSED':
          return h(Tag, {
            color: 'red'
          }, () => '停止')
        case 'COMPLETE':
          return h(Tag, {
            color: 'success'
          }, () => '完成')

        default:
          return record.triggerState
      }
    }
  },
  {
    title: '定时',
    dataIndex: 'triggerType',
    width: 120,
    customRender: ({ record }) => {

      if (record.triggerType == 'CRON') {
        return record.qrtzCronTrigger.cronExpression
      }
      if (record.triggerType == 'SIMPLE') {
        const simple = record.qrtzSimpleTrigger
        return `每${simple.repeatInterval}ms执行一次
        共${simple.timesTriggered}/${simple.repeatCount + 1}次`
      }

      return record.triggerType
    }
  },
  {
    title: '执行时间',
    dataIndex: 'prevFireTime',
    width: 180,
    customRender: ({ record }) => {
      let arr: string[] = []
      if (record.prevFireTime != null) {
        arr.push(`上次：${record.prevFireTime} `)
      }
      if (record.nextFireTime != null) {
        arr.push(`下次：${record.nextFireTime} `)
      }
      return arr.join('\r\n')
    }
  },
  {
    title: '有效周期',
    dataIndex: 'startTime',
    width: 180,
    customRender: ({ record }) => {
      let arr: string[] = []
      if (record.startTime != null) {
        arr.push(`开始：${record.startTime} `)
      }
      if (record.endTime != null) {
        arr.push(`结束：${record.endTime} `)
      }
      return arr.join('\r\n')
    }
  }
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'triggerName',
    label: '计划名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'triggerState',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '停用', value: 1 },
      ],
    },
    colProps: { span: 6 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: '编号',
    component: 'InputNumber',
    show: false,
  },
  {
    field: 'name',
    label: '名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'env',
    label: '环境',
    required: true,
    component: 'Input',
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '停用', value: 1 },
      ],
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
    defaultValue: ''
  }
];
