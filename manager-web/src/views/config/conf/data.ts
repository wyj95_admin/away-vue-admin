import { h } from 'vue';
import { BasicColumn, FormSchema } from '/@/components/Table';
import { Tag } from 'ant-design-vue';
import { CodeEditor, MODE } from '/@/components/CodeEditor';

export const columns: BasicColumn[] = [
  {
    title: '备注',
    dataIndex: 'remark',
    width: 200
  },
  {
    title: '键名',
    dataIndex: 'key',
    width: 200,
  },
  {
    title: '键值',
    dataIndex: 'value',
    width: 180,
  },
  {
    title: '类型',
    dataIndex: 'valueType',
    width: 80,
    customRender: ({ record }) => {
      switch (record.valueType) {
        case 0:
          return h(Tag, { color: 'red' }, () => '值类型');
        case 1:
          return h(Tag, { color: 'blue' }, () => 'JSON');
        case 2:
          return h(Tag, { color: 'default' }, () => 'YAML');
      }
    }
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 120,
    customRender: ({ record }) => {
      const flag = record.status == 0;
      return h(Tag, {
        color: flag ? 'blue' : 'red'
      }, () => flag ? '启用' : '停用');
    }
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  }
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'key',
    label: '键名',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '停用', value: 1 },
      ],
    },
    colProps: { span: 6 },
  },
];


export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: '编号',
    component: 'InputNumber',
    show: false,
  },
  {
    field: 'groupId',
    label: '分组',
    component: 'Select',
    required: true,
    show: true
  },
  {
    field: 'env',
    label: '环境',
    component: 'Input',
    required: true,    
    show: true   
  },
  {
    field: 'key',
    label: '键名',
    required: true,
    component: 'Input',
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '停用', value: 1 },
      ],
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
    defaultValue: ''
  },
  {
    field: 'valueType',
    label: '类型',
    required: true,
    component: 'RadioGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        {
          label: '值对象',
          value: 0,
        },
        {
          label: 'JSON',
          value: 1,
        },
        {
          label: 'YAML',
          value: 2,
        }
      ],
    }
  },
  {
    field: 'value',
    label: '键值',
    required: true,
    component: 'InputTextArea',
    render: ({ model }) => {
      let mode = model.HTML;
      switch (model.valueType) {
        case 0:
          mode = MODE.HTML;
          break;
        case 1:
          mode = MODE.JSON;
          break;
        case 2:
          mode = MODE.YAML;
          break;
      }
      return h(CodeEditor, {
        value: model.value,
        mode: mode,
        onChange: (val) => {
          if (val == "") {
            return
          }
          model.value = val
        }
      });
    }
  }

];
