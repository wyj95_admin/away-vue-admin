import { BasicFetchResult, BasicPageParams } from "../../model/baseModel";

export interface JobModel {
  schedName: string,
  jobName: string,
  jobGroup: string,
  description: string,
  jobClassName: string,
  isDurable: boolean,
  isNonconcurrent: boolean,
  isUpdateData: boolean,
  requestsRecovery: boolean,
  jobData: string
}
export interface JobSearchModel extends BasicPageParams {
  jobName: string,
  jobGroup: string,
  jobClassName: string,
}

export type JobPageModel = BasicFetchResult<JobModel>;