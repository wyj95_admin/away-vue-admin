export interface Key {
  name: string
  group: string
}

export type Keys = Key[];