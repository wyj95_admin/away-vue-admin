import { BasicFetchResult, BasicPageParams } from "../../model/baseModel"

export interface TriggerModel {
  schedName: string,
  triggerName: string,
  triggerGroup: string,
  jobName: string,
  jobGroup: string,
  description: string,
  nextFireTime: string,
  prevFireTime: string,
  priority: number,
  triggerState: string,
  triggerType: string,
  startTime: string,
  endTime: string,
  calendarName: string,
  misfireInstr: number,
  jobData: string,
  qrtzCronTrigger: CronTrigger,
  qrtzSimpleTrigger: SimpleTrigger,
  qrtzSimpropTrigger: SimpropTrigger,
  qrtzBlobTrigger: BlobTrigger
}
export interface CronTrigger {
  cronExpression: string,
  timeZoneId: string
}

export interface SimpleTrigger {
  repeatCount: number,
  repeatInterval: number,
  timesTriggered: number
}
export interface SimpropTrigger {
  strProp1: string,
  strProp2: string,
  strProp3: string,
  intProp1: number,
  intProp2: number,
  longProp1: number,
  longProp2: number,
  decProp1: number,
  decProp2: number,
  boolProp1: boolean,
  boolProp2: boolean,
  timeZoneId: string
}
export interface BlobTrigger {
  blobData: string
}

export interface TriggerSearchModel extends BasicPageParams {
  jobName: string,
  jobGroup: string
}

export type TriggerPageModel = BasicFetchResult<TriggerModel>;