import { Key, Keys } from './model/key';
import { TriggerModel, TriggerPageModel, TriggerSearchModel } from './model/triggerModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  GetTriggerGroupList = '/trigger/list',
  GetTriggerPage = '/trigger',
  AddTrigger = '/trigger',
  UpdateTrigger = '/trigger',
  RemovTrigger = '/trigger',
  StartTriggerGroup = '/trigger/group/start',
  StopTriggerGroup = '/trigger/group/stop',
  StartTrigger = '/trigger/start',
  StopTrigger = '/trigger/stop',
  RunTrigger = '/trigger/run'
}
export const getTriggerGroupList = () => {
  return defHttp.get<string[]>({ url: Api.GetTriggerGroupList });
}
export const getTriggerPage = (params: TriggerSearchModel) => {
  return defHttp.get<TriggerPageModel>({ url: Api.GetTriggerPage, params });
}
export const addTrigger = (params: TriggerModel) => {
  return defHttp.post({ url: Api.AddTrigger, params });
}
export const updateTrigger = (params: TriggerModel) => {
  return defHttp.put({ url: Api.UpdateTrigger, params });
}
export const removTrigger = (params: Keys) => {
  return defHttp.delete({ url: Api.RemovTrigger, params });
}
export const startTriggerGroup = (group: string) => {
  return defHttp.get({ url: Api.StartTriggerGroup, params: { group } });
}
export const stopTriggerGroup = (group: string) => {
  return defHttp.get({ url: Api.StopTriggerGroup, params: { group } });
}
export const startTrigger = (params: Keys) => {
  return defHttp.post({ url: Api.StartTrigger, params });
}
export const stopTrigger = (params: Keys) => {
  return defHttp.post({ url: Api.StopTrigger, params });
}
export const runTrigger = (params: Key) => {
  return defHttp.post({ url: Api.RunTrigger, params });
}
