import { ConfModel } from '../config/model/confModel';
import { JobModel, JobPageModel, JobSearchModel } from './model/jobModel';
import { Keys, Key } from './model/key';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  GetJobGroupList = '/job/group/list',
  GetJobPage = '/job',
  GetJobList = '/job/list',
  AddJob = '/job',
  UpdateJob = '/job',
  RemoveJob = '/job',
  StartJobGroup = '/job/group/start',
  StopJobGroup = '/job/group/stop',
  StartJob = '/job/start',
  StopJob = '/job/stop',
  RunJob = '/job/run',
  CancelJob = '/job/cancel'
}

export const getJobGroupList = () => {
  return defHttp.get<string[]>({ url: Api.GetJobGroupList });
}
export const getJobPage = (params: JobSearchModel) => {
  return defHttp.get<JobPageModel>({ url: Api.GetJobPage, params });
}
export const addJob = (params: JobModel) => {
  return defHttp.post({ url: Api.AddJob, params });
}
export const updateJob = (params: ConfModel) => {
  return defHttp.put({ url: Api.UpdateJob, params });
}
export const removeJob = (params: Keys) => {
  return defHttp.delete({ url: Api.RemoveJob, params });
}
export const startJobGroup = (group: string) => {
  return defHttp.get({ url: Api.StartJobGroup, params: { group } });
}
export const stopJobGroup = (group: string) => {
  return defHttp.get({ url: Api.StopJobGroup, params: { group } });
}
export const startJob = (params: Keys) => {
  return defHttp.post({ url: Api.StartJob, params });
}
export const stopJob = (params: Keys) => {
  return defHttp.post({ url: Api.StopJob, params });
}
export const runJob = (params: Key) => {
  return defHttp.post({ url: Api.RunJob, params });
}
export const cancelJob = (params: Key) => {
  return defHttp.post({ url: Api.CancelJob, params });
}