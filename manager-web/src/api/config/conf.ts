import { ConfModel, ConfPageModel, ConfSearchModel } from './model/confModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  AddConf = '/sys/conf/config',
  UpdateConf = '/sys/conf/config',
  RemovConf = '/sys/conf/config',
  GetConfPage = '/sys/conf/config',
}


export const addConf = (params: ConfModel) => {
  return defHttp.post({ url: Api.AddConf, params });
}
export const updateConf = (params: ConfModel) => {
  return defHttp.put({ url: Api.UpdateConf, params });
}
export const removConf = (params: number[]) => {
  return defHttp.delete({ url: Api.RemovConf, params });
}
export const getConfPage = (params: ConfSearchModel) => {
  return defHttp.get<ConfPageModel>({ url: Api.GetConfPage, params });
}

