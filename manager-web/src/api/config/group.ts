import { GroupModel, GroupPageModel, GroupSearchModel } from './model/groupModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  AddGroup = '/sys/conf/group',
  UpdateGroup = '/sys/conf/group',
  RemovGroup = '/sys/conf/group',
  GetGroupPage = '/sys/conf/group',
  GetGroupList = '/sys/conf/group/list',
  GetEnvList = '/sys/conf/env/list',
}


export const addGroup = (params: GroupModel) => {
  return defHttp.post({ url: Api.AddGroup, params });
}
export const updateGroup = (params: GroupModel) => {
  return defHttp.put({ url: Api.UpdateGroup, params });
}
export const removGroup = (params: number[]) => {
  return defHttp.delete({ url: Api.RemovGroup, params });
}
export const getGroupPage = (params: GroupSearchModel) => {
  return defHttp.get<GroupPageModel>({ url: Api.GetGroupPage, params });
}
export const getGroupList = () => {
  return defHttp.get<GroupModel[]>({ url: Api.GetGroupList });
}
export const getEnvList = () => {
  return defHttp.get<string[]>({ url: Api.GetEnvList });
}
