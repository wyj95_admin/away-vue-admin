import { BasicFetchResult, BasicPageParams } from "../../model/baseModel"

export interface GroupModel {
  id: number
  name: string
  remark: string
  createTime: string
  env: string
  status: number
}

export interface GroupSearchModel extends BasicPageParams {
  name: string,
  env: string,
  status: number,
}

export type GroupPageModel = BasicFetchResult<GroupModel>;