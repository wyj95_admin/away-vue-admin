import { BasicFetchResult, BasicPageParams } from "../../model/baseModel"

export interface ConfModel {
  id: number
  remark: string
  key: string
  value: string
  groupId: number
  createTime: string
  updateTime: string
  status: number
  valueType: number
}

export interface ConfSearchModel extends BasicPageParams {
  name: string,
  env: string,
  groupId: number,
  status: number,
}

export type ConfPageModel = BasicFetchResult<ConfModel>;