import { ApiModel, ApiPageModel, ApiSearchModel, ApiTreeModel } from './model/apiModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  ImportSwaggerApi = '/sys/api_resource/import_swagger',
  AddApi = '/sys/api_resource',
  UpdateApi = '/sys/api_resource',
  RemoveApi = '/sys/api_resource',
  GetApiPage = '/sys/api_resource',
  GetApiTree = '/sys/api_resource/tree',
}

export const importSwaggerApi = (val: string) => {
  return defHttp.post({ url: Api.ImportSwaggerApi, data: val });
}
export const addApi = (params: ApiModel) => {
  return defHttp.post({ url: Api.AddApi, params });
}
export const updateApi = (params: ApiModel) => {
  return defHttp.put({ url: Api.UpdateApi, params });
}
export const removeApi = (params: number[]) => {
  return defHttp.delete({ url: Api.RemoveApi, params });
}
export const getApiPage = (params: ApiSearchModel) => {
  return defHttp.get<ApiPageModel>({ url: Api.GetApiPage, params });
}
export const getApiTree = () => {
  return defHttp.get<ApiTreeModel>({ url: Api.GetApiTree });
}
