import { getMenuListResultModel } from '../sys/model/menuModel';
import { ViewModel, ViewPremModel, ViewTreeModel } from './model/viewModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  ImportView = '/sys/view_resource/import',
  AddView = '/sys/view_resource',
  UpdateView = '/sys/view_resource',
  RemoveView = '/sys/view_resource',
  GetViewTree = '/sys/view_resource/tree',
  GetViewPremTree = '/sys/view_resource/and_prem/tree',

  AddViewPrem = '/sys/view_resource/prem_code',
  UpdateViewPrem = '/sys/view_resource/prem_code',
  RemoveViewPrem = '/sys/view_resource/prem_code',
  GetViewPremList = '/sys/view_resource/prem_code'
}

export const importView = (params: getMenuListResultModel) => {
  return defHttp.post({ url: Api.ImportView, params });
}
export const addView = (params: ViewModel) => {
  return defHttp.post({ url: Api.AddView, params });
}
export const updateView = (params: ViewModel) => {
  return defHttp.put({ url: Api.UpdateView, params });
}
export const removeView = (params: number[]) => {
  return defHttp.delete({ url: Api.RemoveView, params });
}
export const getViewTree = () => {
  return defHttp.get<ViewTreeModel>({ url: Api.GetViewTree });
}
export const getViewPremTree = () => {
  return defHttp.get<ViewTreeModel>({ url: Api.GetViewPremTree });
}


export const addViewPrem = (params: ViewPremModel) => {
  return defHttp.post({ url: Api.AddViewPrem, params });
}
export const updateViewPrem = (params: ViewPremModel) => {
  return defHttp.put({ url: Api.UpdateViewPrem, params });
}
export const removeViewPrem = (params: number[]) => {
  return defHttp.delete({ url: Api.RemoveViewPrem, params });
}
export const getViewPremList = (viewId: number) => {
  return defHttp.get<ViewPremModel[]>({ url: Api.GetViewPremList, params: { viewId } });
}