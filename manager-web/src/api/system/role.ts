import { RoleModel, RolePageModel, RoleSearchModel, SetPrem } from './model/roleModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  AddRole = '/sys/role',
  UpdateRole = '/sys/role',
  RemoveRole = '/sys/role',
  GetRolePage = '/sys/role',
  GetRoleTree = '/sys/role/tree',
  SetRoleView = '/sys/role/permission/view_resource',
  SetRoleApi = '/sys/role/permission/api_resource'
}

export const addRole = (params: RoleModel) => {
  return defHttp.post({ url: Api.AddRole, params });
}
export const updateRole = (params: RoleModel) => {
  return defHttp.put({ url: Api.UpdateRole, params });
}
export const removeRole = (params: number[]) => {
  return defHttp.delete({ url: Api.RemoveRole, params });
}
export const getRolePage = (params?: RoleSearchModel) => {
  return defHttp.get<RolePageModel>({ url: Api.GetRolePage, params });
}
export const getRoleTree = () => {
  return defHttp.get({ url: Api.GetRoleTree });
}
export const setRoleView = (params: SetPrem) => {
  return defHttp.post({ url: Api.SetRoleView, params });
}
export const setRoleApi = (params: SetPrem) => {
  return defHttp.post({ url: Api.SetRoleApi, params });
}
