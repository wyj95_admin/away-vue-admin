import { DeptModel, DeptSearchModel, DeptTreeModel } from './model/deptModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  AddOrg = '/sys/organization',
  UpdateOrg = '/sys/organization',
  RemoveOrg = '/sys/organization',
  GetOrgTree = '/sys/organization/tree'
}

export const addOrg = (params: DeptModel) => {
  return defHttp.post({ url: Api.AddOrg, params });
}

export const updateOrg = (params: DeptModel) => {
  return defHttp.put({ url: Api.UpdateOrg, params });
}

export const removeOrg = (params: number[]) => {
  return defHttp.delete({ url: Api.RemoveOrg, params });
}

export const getOrgTree = (params?: DeptSearchModel) => {
  return defHttp.get<DeptTreeModel>({ url: Api.GetOrgTree, params });
}
