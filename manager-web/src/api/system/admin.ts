import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';
import {
  AddAdminModel,
  AdminInfoModel,
  AdminModel,
  AdminPageModel,
  AdminSearchModel,
  LoginModel,
  LoginResultModel,
  MenuTreeModel
} from './model/adminModel';
import { SetPrem } from './model/roleModel';

enum Api {
  Login = '/sys/admin/login',
  Logout = '/sys/admin/logout',

  GetUserInfo = '/sys/admin/info',
  GetPermCode = '/sys/admin/perm_code',
  GetViews = '/sys/admin/views',

  AddAdmin = '/sys/admin',
  UpdateAdmin = '/sys/admin',
  RemoveAdmin = '/sys/admin',
  GetAdminPage = '/sys/admin',

  SetView = '/sys/admin/permission/view_resource',
  SetApi = '/sys/admin/permission/api_resource',
  SetOrg = '/sys/admin/permission/organization',
  SetRole = '/sys/admin/permission/role'
}

export function login(params: LoginModel, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<LoginResultModel>(
    {
      url: Api.Login,
      params
    },
    {
      errorMessageMode: mode
    }
  );
}
export function logout() {
  return defHttp.get({ url: Api.Logout });
}

export function getUserInfo() {
  return defHttp.get<AdminInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}
export function getPermCode() {
  return defHttp.get<string[]>({ url: Api.GetPermCode });
}
export const getViews = () => {
  return defHttp.get<MenuTreeModel>({ url: Api.GetViews });
};

export const addAdmin = (params: AddAdminModel) => {
  return defHttp.post({ url: Api.AddAdmin, params });
};

export const updateAdmin = (params: AdminModel) => {
  return defHttp.put({ url: Api.UpdateAdmin, params });
};

export const removeAdmin = (params: number[]) => {
  return defHttp.delete({ url: Api.RemoveAdmin, params });
};

export const getAdminPage = (params?: AdminSearchModel) => {
  return defHttp.get<AdminPageModel>({ url: Api.GetAdminPage, params });
};

export const setAdminView = (params: SetPrem) => {
  return defHttp.post({ url: Api.SetView, params });
};
export const setAdminApi = (params: SetPrem) => {
  return defHttp.post({ url: Api.SetApi, params });
};
export const setAdminRole = (params: SetPrem) => {
  return defHttp.post({ url: Api.SetRole, params });
};
export const setAdminOrg = (params: SetPrem) => {
  return defHttp.post({ url: Api.SetOrg, params });
};
