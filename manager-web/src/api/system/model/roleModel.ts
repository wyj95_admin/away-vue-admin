import { BasicFetchResult, BasicPageParams } from "../../model/baseModel";

export interface RoleModel {
  id: number,
  roleName: string,
  roleValue: string,
  orderNo: number,
  status: number,
  remark: string,
  createTime: string
}

export interface RoleSearchModel extends BasicPageParams {
  roleName: string,
  roleValue: string,
  status: number,
}

export type RolePageModel = BasicFetchResult<RoleModel>;

export interface SetPrem {
  oid: number,
  mid: number[]
}