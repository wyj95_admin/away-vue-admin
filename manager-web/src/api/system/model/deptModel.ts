export interface DeptModel {
  id: number,
  deptName: string,
  status: number,
  parentId: number,
  orderNo: number,
  remark: string,
  createTime: string
}

export interface DeptTreeModel extends DeptModel {
  children?: DeptTreeModel
}

export interface DeptSearchModel {
  deptName: string,
  status: number
}