import { BasicFetchResult, BasicPageParams } from "../../model/baseModel";

export interface ApiModel {
  id: number,
  createTime: string,
  url: string,
  apiName: string,
  method: string,
  group: string
}

export interface ApiSearchModel extends BasicPageParams {
  id: number,
  url: string,
  apiName: string,
  group: string
}

export type ApiPageModel = BasicFetchResult<ApiModel>;

export interface ApiTreeModel {
  id: number,
  parentId: number,
  apiName: string,
  children?: ApiTreeModel
}
