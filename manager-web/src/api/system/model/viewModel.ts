export interface ViewModel {
  id: number,
  parentId: number,
  name: string,
  path: string,
  component: string,
  alias: string,
  status: 0,
  createTime: string,
  redirect: string,
  caseSensitive: Boolean,
  meta: string
}

export interface ViewTreeModel extends ViewModel {
  children?: ViewTreeModel
}

export interface ViewPremModel {
  id: number,
  viewId: number,
  premCode: string,
  permName: string
}