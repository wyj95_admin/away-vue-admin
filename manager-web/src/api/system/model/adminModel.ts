import { RouteMeta } from 'vue-router';
import { BasicFetchResult, BasicPageParams } from '../../model/baseModel';

export interface AddAdminModel {
  username: string;
  password: string;
}

export type LoginModel = AdminModel;

export interface LoginResultModel {
  access_token: string;
  token_type: string;
  expires: number;
}
export interface RoleInfo {
  roleName: string;
  roleValue: string;
}

export interface RouteItem {
  path: string;
  component: any;
  meta: RouteMeta;
  name?: string;
  alias?: string | string[];
  redirect?: string;
  caseSensitive?: boolean;
  children?: RouteItem[];
}

export type MenuTreeModel = RouteItem[];

export interface AdminInfoModel {
  roles: RoleInfo[];
  orgs?: string[];
  // 用户id
  userId: string | number;
  // 用户名
  username: string;
  // 真实名字
  realname: string;
  // 头像
  avatar: string;
  // 介绍
  desc?: string;
}

export interface AdminModel {
  id: number;
  nickname: string;
  username: string;
  realname: string;
  avatar: string;
  desc: string;
  createTime: string;
}

export interface AdminSearchModel extends BasicPageParams {
  orgId: number;
  username: string;
  nickname: string;
  realname: string;
}

export type AdminPageModel = BasicFetchResult<AdminModel>;
