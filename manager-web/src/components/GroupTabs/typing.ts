export default interface GroupTabsActionType {
  reload: () => Promise<void>
}