import type { App } from 'vue';
import { Button } from './Button';
import { Input, Layout, Space, Tag, List, Menu, Tabs } from 'ant-design-vue';
import VXETable from 'vxe-table';

export function registerGlobComp(app: App) {
  app
    .use(Tabs)
    .use(Menu)
    .use(Tag)
    .use(List)
    .use(Space)
    .use(Input)
    .use(Button)
    .use(Layout)
    .use(VXETable);
}
