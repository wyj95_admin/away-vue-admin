#!/bin/bash
set -e
set -x

Build_Dir='../dist/web'
Image_Name="registry.cn-shenzhen.aliyuncs.com/1461076997/adminweb:latest"

pub() {
    rm -rf ${Build_Dir}
    mkdir -p ${Build_Dir}
    pnpm i
    pnpm run build
    cp -rf ./dist "${Build_Dir}/dist"
}

  #  location /api {
  #       proxy_set_header Host $http_host;
  #       proxy_set_header  X-Real-IP $remote_addr;
  #       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  #       proxy_set_header X-Forwarded-Proto $scheme;
  #       rewrite ^/api/(.*)$ /$1 break;
  #       proxy_pass http://localhost:9000;
  #   }

img() {
    echo '
server {
    listen       80;
    server_name  localhost;   

    location / {
        root /app;
        index  index.html;
        try_files $uri $uri/ /index.html;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
' >"${Build_Dir}/default.conf"

    echo '
FROM nginx:alpine
RUN apk add --no-cache tzdata
ENV TZ Asia/Shanghai
COPY ./default.conf /etc/nginx/conf.d
COPY ./dist /app

EXPOSE 80
' >"${Build_Dir}/dockerfile"

    echo 'Dockerfile' >"${Build_Dir}/.dockerignore"
    cd ${Build_Dir} && docker build -t ${Image_Name} .
}

run() {
    pub
    img
}

push(){
  docker push ${Image_Name}
}

$@