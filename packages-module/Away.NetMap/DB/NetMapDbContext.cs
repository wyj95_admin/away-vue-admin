﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Away.NetMap.DB;

public partial class NetMapDbContext : DbContext
{
    public NetMapDbContext(DbContextOptions<NetMapDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<NetIp> NetIps { get; set; }

    public virtual DbSet<NetPort> NetPorts { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb4_general_ci")
            .HasCharSet("utf8mb4");

        modelBuilder.Entity<NetIp>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("net_ip", tb => tb.HasComment("IP信息表"));

            entity.HasIndex(e => e.Ip, "ip").IsUnique();

            entity.Property(e => e.Id)
                .HasComment("网络地址")
                .HasColumnName("id");
            entity.Property(e => e.Address)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("物理地址")
                .HasColumnName("address");
            entity.Property(e => e.Coord)
                .HasMaxLength(255)
                .HasComment("坐标（经纬度）")
                .HasColumnName("coord");
            entity.Property(e => e.Country)
                .HasMaxLength(255)
                .HasComment("国家")
                .HasColumnName("country");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.Ip)
                .HasMaxLength(15)
                .HasDefaultValueSql("''")
                .HasComment("网络地址")
                .HasColumnName("ip");
            entity.Property(e => e.UpdateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("更新时间")
                .HasColumnType("datetime")
                .HasColumnName("update_time");
        });

        modelBuilder.Entity<NetPort>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("net_port", tb => tb.HasComment("网络端口服务"));

            entity.HasIndex(e => new { e.Ip, e.Port }, "ip").IsUnique();

            entity.Property(e => e.Id)
                .HasComment("主键")
                .HasColumnName("id");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.Ip)
                .HasMaxLength(15)
                .HasComment("网络地址")
                .HasColumnName("ip");
            entity.Property(e => e.Port)
                .HasComment("网络端口")
                .HasColumnName("port");
            entity.Property(e => e.Service)
                .HasMaxLength(255)
                .HasComment("服务类型")
                .HasColumnName("service");
            entity.Property(e => e.UpdateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("更新时间")
                .HasColumnType("datetime")
                .HasColumnName("update_time");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
