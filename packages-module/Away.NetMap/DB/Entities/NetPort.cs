﻿using System;
using System.Collections.Generic;

namespace Away.NetMap.DB;

/// <summary>
/// 网络端口服务
/// </summary>
public partial class NetPort
{
    /// <summary>
    /// 网络地址
    /// </summary>
    public string Ip { get; set; } = null!;

    /// <summary>
    /// 网络端口
    /// </summary>
    public int Port { get; set; }

    /// <summary>
    /// 服务类型
    /// </summary>
    public string? Service { get; set; }

    /// <summary>
    /// 主键
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime? CreateTime { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    public DateTime? UpdateTime { get; set; }
}
