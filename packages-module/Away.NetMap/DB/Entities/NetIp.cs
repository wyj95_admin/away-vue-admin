﻿using System;
using System.Collections.Generic;

namespace Away.NetMap.DB;

/// <summary>
/// IP信息表
/// </summary>
public partial class NetIp
{
    /// <summary>
    /// 网络地址
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 物理地址
    /// </summary>
    public string? Address { get; set; }

    /// <summary>
    /// 国家
    /// </summary>
    public string? Country { get; set; }

    /// <summary>
    /// 坐标（经纬度）
    /// </summary>
    public string? Coord { get; set; }

    /// <summary>
    /// 网络地址
    /// </summary>
    public string Ip { get; set; } = null!;

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime? CreateTime { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    public DateTime? UpdateTime { get; set; }
}
