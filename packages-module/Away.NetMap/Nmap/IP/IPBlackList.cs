﻿namespace Away.NetMap.Nmap;

///1、A类：地址范围是1.0.0.0 到 127.255.255.255，主要分配 给大量主机而局域网网络数量较少的大型网络；
///2、B类：地址范围是128.0.0.0 到191.255.255.255，一般用于国际性大公司和政府机构；
///3、C类：地址范围是192.0.0.0 到223.255.255.255，用于一般小公司校园网研究机构等；
///4、D类：地址范围是224.0.0.0 到 239.255.255.255，用于特殊用途，又称做广播地址；
///5、E类：地址范围是240.0.0.0 到255.255.255.255，暂时保留。
///这三个范围的私有IP：
///10.0.0.0--10.255.255.255
///172.16.0.0--172.31.255.255
///192.168.0.0--192.168.255.255
public static class IPBlackList
{
    public static (int, int) PublicIPSection_A => Section("1.0.0.0 ", " 127.255.255.255");
    public static (int, int) PublicIPSection_B => Section("128.0.0.0 ", "191.255.255.255");
    public static (int, int) PublicIPSection_C => Section("192.0.0.0", "223.255.255.255");
    public static (int, int) PublicIPSection_D => Section("224.0.0.0 ", "239.255.255.255");
    public static (int, int) PublicIPSection_E => Section("240.0.0.0 ", "255.255.255.255");

    public static readonly List<(int, int)> PublicIPSections = new()
    {
        PublicIPSection_A,
        PublicIPSection_B,
        PublicIPSection_C
    };

    public static readonly List<(int, int)> PrivateIPSections = new()
    {
        Section("10.0.0.0","10.255.255.255"),
        Section("172.16.0.0","172.31.255.255"),
        Section("192.168.0.0","192.168.255.255")
    };

    public static bool IsPrivateIP(int ip)
    {
        return PrivateIPSections.Any(o => o.Item1 <= ip && o.Item2 >= ip);
    }

    public static bool IsPublicIP(int ip)
    {
        return PublicIPSections.Any(o => !IsPrivateIP(ip));
    }

    private static (int, int) Section(string section1, string section2)
    {
        var start = IPHelper.ToInt(section1);
        var end = IPHelper.ToInt(section2);
        return (start, end);
    }
}
