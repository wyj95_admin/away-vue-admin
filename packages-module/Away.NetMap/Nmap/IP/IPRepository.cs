﻿namespace Away.NetMap.Nmap;

[ServiceInject(ServiceLifetime.Scoped, true)]
public class IPRepository(NetMapDbContext context) : NetMapRepository(context)
{
    public void AddIp(string ip)
    {
        if (Query<NetIp>().Any(o => o.Ip == ip))
        {
            return;
        }
        Add(new NetIp { Ip = ip });
        Save();
    }
}
