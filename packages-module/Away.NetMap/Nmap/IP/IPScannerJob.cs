﻿namespace Away.NetMap.Nmap;

[Job(nameof(IPScannerJob), "netmap", "网络地址扫描任务")]
[Trigger("a", "0 0 0 1/5 * ? ", "start=1.0.0.0&end=127.255.255.255&threads=100", "A类IP段扫描")]
[Trigger("b", "0 0 0 1/5 * ? ", "start=128.0.0.0&end=191.255.255.255&threads=100", "B类IP段扫描")]
[Trigger("c", "0 0 0 1/5 * ? ", "start=192.0.0.0&end=223.255.255.255&threads=100", "C类IP段扫描")]
public class IPScannerJob : IJob
{
    private readonly IPScannerService _scannerService;

    public IPScannerJob(IPScannerService scannerService)
    {
        _scannerService = scannerService;
    }

    public Task Execute(IJobExecutionContext context)
    {
        var p = context.MergedJobDataMap.ToModel<IPScannerParams>() ?? throw new ArgumentNullException(nameof(IPScannerParams));
        _scannerService.Run(p, context.CancellationToken);
        return Task.CompletedTask;
    }
}
