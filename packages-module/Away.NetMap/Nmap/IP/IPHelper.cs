﻿using System.Net;

namespace Away.NetMap.Nmap;

public static class IPHelper
{
    public static int ToInt(IPAddress ip)
    {
        return ToInt(ip.GetAddressBytes());
    }

    public static int ToInt(byte[] ipbytes)
    {
        return BitConverter.ToInt32(ipbytes.Reverse().ToArray());
    }

    public static int ToInt(string ip)
    {
        return ToInt(IPAddress.Parse(ip));
    }

    public static IPAddress ToIPAddress(int num)
    {
        return new IPAddress(BitConverter.GetBytes(num).Reverse().ToArray());
    }

    public static string ToString(int num)
    {
        return ToIPAddress(num).ToString();
    }
}
