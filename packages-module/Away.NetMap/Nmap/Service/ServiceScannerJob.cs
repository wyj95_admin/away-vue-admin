﻿namespace Away.NetMap.Nmap;

[Job(nameof(ServiceScannerJob), "netmap", "网络服务扫描任务")]
[Trigger("1", "0 0 0 1/5 * ? ", "ip=192.168.64.201&port=22&threads=100", "测试ssh服务扫描")]
[Trigger("2", "0 0 0 1/5 * ? ", "ip=192.168.64.201&port=80&threads=100", "测试http服务扫描")]
[Trigger("3", "0 0 0 1/5 * ? ", "ip=192.168.64.201&port=3306&threads=100", "测试mysql服务扫描")]
[Trigger("4", "0 0 0 1/5 * ? ", "ip=192.168.64.201&port=5432&threads=100", "测试postgresql服务扫描")]
public class ServiceScannerJob(ServiceScannerService scannerService) : IJob
{
    private readonly ServiceScannerService _scannerService = scannerService;

    public Task Execute(IJobExecutionContext context)
    {
        var p = context.MergedJobDataMap.ToModel<ServiceScannerParams>() ?? throw new ArgumentNullException(nameof(ServiceScannerParams));
        _scannerService.Run(p, context.CancellationToken);
        return Task.CompletedTask;
    }
}
