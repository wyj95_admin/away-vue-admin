﻿namespace Away.NetMap.Nmap;

[Job(nameof(NmapJob), "netmap", "nmap扫描任务")]
[Trigger("a", "0 0 0 1/5 * ? ", "start=1.0.0.0&end=127.255.255.255&threads=10", "A类IP段扫描")]
[Trigger("b", "0 0 0 1/5 * ? ", "start=128.0.0.0&end=191.255.255.255&threads=10", "B类IP段扫描")]
[Trigger("c", "0 0 0 1/5 * ? ", "start=192.0.0.0&end=223.255.255.255&threads=10", "C类IP段扫描")]
public class NmapJob(NmapService nmapService) : IJob
{
    private readonly NmapService _nmapService = nmapService;

    public Task Execute(IJobExecutionContext context)
    {
        var p = context.MergedJobDataMap.ToModel<NmapParams>() ?? throw new ArgumentNullException(nameof(NmapParams));
        _nmapService.Run(p, context.CancellationToken);
        return Task.CompletedTask;
    }
}
