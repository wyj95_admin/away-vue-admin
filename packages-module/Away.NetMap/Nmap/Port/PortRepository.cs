﻿namespace Away.NetMap.Nmap;

[ServiceInject(ServiceLifetime.Scoped, true)]
public class PortRepository : NetMapRepository
{
    public PortRepository(NetMapDbContext context) : base(context)
    {
    }

    public void Update(string ip, int port, string? service = null)
    {
        var entity = Query<NetPort>().FirstOrDefault(o => o.Ip == ip && o.Port == port);
        if (entity == null)
        {
            Add(new NetPort { Ip = ip, Port = port });
            return;
        }

        if (service != null)
        {
            entity.Service = service;
        }

        Save();
    }
}
