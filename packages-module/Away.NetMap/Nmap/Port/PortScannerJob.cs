﻿namespace Away.NetMap.Nmap;

[Job(nameof(PortScannerJob), "netmap", "网络端口扫描任务")]
[Trigger("1", "0 0 0 1/5 * ? ", "ip=1.0.0.20&threads=100&timeout=1000", "端口扫描")]
public class PortScannerJob : IJob
{
    private readonly PortScannerService _scannerService;

    public PortScannerJob(PortScannerService scannerService)
    {
        _scannerService = scannerService;
    }

    public Task Execute(IJobExecutionContext context)
    {
        var p = context.MergedJobDataMap.ToModel<PortScannerParams>() ?? throw new ArgumentNullException(nameof(PortScannerParams));
        _scannerService.Run(p, context.CancellationToken);
        return Task.CompletedTask;
    }
}
