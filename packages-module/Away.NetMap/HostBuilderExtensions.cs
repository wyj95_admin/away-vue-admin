﻿using Away.Common.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Away.NetMap;

public static class HostBuilderExtensions
{
    /// <summary>
    /// 添加网络空间模块
    /// </summary>
    /// <param name="builder"></param>
    public static void AddNetMap(this IHostBuilder builder)
    {
        builder.ConfigureServices((host, services) =>
        {            
            var conn = host.Configuration.GetConnectionString("MySql");
            var ver = ServerVersion.Parse("8.0.26");
            services.AddDbContext<NetMapDbContext>(o => o.UseMySql(conn, ver));
            services.AddHostedService<InitTablesHostService<NetMapDbContext>>();
        });
    }
}
