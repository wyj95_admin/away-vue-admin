﻿namespace Away.Admin.Services.Models;

public class ApiResourceTree : TreeBase<ApiResourceTree>
{
    /// <summary>
    /// 接口名称
    /// </summary>
    public string ApiName { get; set; } = null!;
    public bool Disabled { get; set; }
}
