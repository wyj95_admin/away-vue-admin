﻿namespace Away.Admin.Services.Models;

public class Login
{
    private string _username = string.Empty;
    private string _password = string.Empty;

    [Required]
    public required string Username
    {
        get => string.IsNullOrWhiteSpace(_username) ? string.Empty : _username.Trim();
        set => _username = value;
    }

    [Required]
    public required string Password
    {
        get => string.IsNullOrWhiteSpace(_password) ? string.Empty : _password.Trim();
        set => _password = value;
    }

    public string PasswordMd5(string salt)
    {
        return Md5Builder.Build($"{Password}_{salt}");
    }
}
