﻿namespace Away.Admin.Services.Models;

public class MetaConvert
{
    public string Icon => GetToString("icon");
    public string Title => GetToString("title");

    private readonly Dictionary<string, object> _meta;
    public MetaConvert(string jsonvalue)
    {
        if (string.IsNullOrWhiteSpace(jsonvalue))
        {
            _meta = new Dictionary<string, object>();
        }
        _meta = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonvalue) ?? new Dictionary<string, object>();
    }

    public object? Get(string key)
    {
        _meta.TryGetValue(key, out object? val);
        return val;
    }

    public string GetToString(string key)
    {
        return Convert.ToString(Get(key)) ?? string.Empty;
    }
}
