﻿namespace Away.Admin.Services.Models;

public class RoleTree : TreeBase<RoleTree>
{
    /// <summary>
    /// 角色名称
    /// </summary>
    public string Name { get; set; } = null!;

    public bool Disabled { get; set; }
}
