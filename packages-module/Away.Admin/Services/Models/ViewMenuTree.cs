﻿namespace Away.Admin.Services.Models;

public class ViewMenuTree : TreeBase<ViewMenuTree>
{
    /// <summary>
    /// 编号
    /// </summary>
    [JsonIgnore]
    public override int Id { get; set; }

    /// <summary>
    /// 上级编号
    /// </summary>
    [JsonIgnore]
    public override int ParentId { get; set; }

    /// <summary>
    /// 菜单名称
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// 路由地址
    /// </summary>
    public string Path { get; set; } = null!;

    /// <summary>
    /// 组件路径
    /// </summary>
    public string Component { get; set; } = null!;

    /// <summary>
    /// 路由别名
    /// </summary>
    public string? Alias { get; set; }

    /// <summary>
    /// 跳转地址
    /// </summary>
    public string? Redirect { get; set; }

    public bool CaseSensitive { get; set; }

    public Dictionary<string, object> Meta { get; set; } = null!;
}