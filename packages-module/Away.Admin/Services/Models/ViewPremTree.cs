﻿namespace Away.Admin.Services.Models;

public class ViewPremTree : TreeBase<ViewPremTree>
{
    public required string Title { get; set; }
    public required string Icon { get; set; }
}
