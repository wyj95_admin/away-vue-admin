﻿namespace Away.Admin.Services.Models;

public class ViewResourceTree : TreeBase<ViewResourceTree>
{
    [JsonIgnore]
    public MetaConvert Metas => new(Meta);

    /// <summary>
    /// 图标
    /// </summary>
    public string Icon => Metas.Icon;
    /// <summary>
    /// 菜单名称
    /// </summary>
    public string Title => Metas.Title;

    /// <summary>
    /// 路由名称
    /// </summary>
    public required string Name { get; set; }

    /// <summary>
    /// 路由地址
    /// </summary>
    public required string Path { get; set; }

    /// <summary>
    /// 组件路径
    /// </summary>
    public required string Component { get; set; }

    /// <summary>
    /// 路由别名
    /// </summary>
    public required string Alias { get; set; }

    /// <summary>
    /// 状态:0启用；1禁用
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 跳转地址
    /// </summary>
    public required string Redirect { get; set; }

    public bool CaseSensitive { get; set; }

    public required string Meta { get; set; }
}
