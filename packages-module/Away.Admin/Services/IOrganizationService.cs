﻿namespace Away.Admin.Services;

/// <summary>
/// 组织架构服务
/// </summary>
public interface IOrganizationService
{
    /// <summary>
    /// 添加部门
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddOrganization(SysOrganization model);
    /// <summary>
    /// 修改部门
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateOrganization(SysOrganization model);
    /// <summary>
    /// 删除部门
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveOrganization([Required] int[] ids);
    /// <summary>
    /// 获取部门树形列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    List<OrganizationTree> GetOrganizationTree(OrganizationSearch search);
}
