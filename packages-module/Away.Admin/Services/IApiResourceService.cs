﻿namespace Away.Admin.Services;

/// <summary>
/// 后端Api资源服务
/// </summary>
public interface IApiResourceService
{
    /// <summary>
    /// 添加接口资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddApiResource(SysApiResource model);
    /// <summary>
    /// 修改接口资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateApiResource(SysApiResource model);
    /// <summary>
    /// 删除接口资源
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveApiResource([Required] int[] ids);
    /// <summary>
    /// 获取接口资源分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetApiResourcePage(ApiResourceSearch search);
    /// <summary>
    /// 导入swagger接口数据
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    Task ImportSwaggerApi(string url);
    /// <summary>
    /// 获取接口资源树形列表
    /// </summary>
    /// <returns></returns>
    List<ApiResourceTree> GetApiResourceTree();
}
