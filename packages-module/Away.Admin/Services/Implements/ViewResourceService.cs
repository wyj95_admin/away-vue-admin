﻿namespace Away.Admin.Services.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class ViewResourceService : IViewResourceService
{
    private readonly IViewResourceRepository _viewResourceRepository;
    public ViewResourceService(IViewResourceRepository viewResourceRepository)
    {
        _viewResourceRepository = viewResourceRepository;
    }

    public bool AddViewResource(SysViewResource model)
    {
        return _viewResourceRepository.AddViewResource(model);
    }

    public bool AddViewResourcePrem(SysViewResourcePrem model)
    {
        return _viewResourceRepository.AddViewResourcePrem(model);
    }

    public List<ViewResourceTree> GetViewResourceTree()
    {
        var list = _viewResourceRepository.GetViewResourceList();
        var items = list.Select(o => new ViewResourceTree
        {
            Id = o.Id,
            ParentId = o.ParentId,
            CreateTime = o.CreateTime,
            Alias = o.Alias,
            CaseSensitive = o.CaseSensitive == 0,
            Component = o.Component,
            Meta = o.Meta,
            Name = o.Name,
            Path = o.Path,
            Redirect = o.Redirect,
            Status = o.Status
        }).ToList();

        return TreeBuilder.Build(items);
    }

    void AddTree(ViewMenuTree tree)
    {
        var entity = new SysViewResource
        {
            ParentId = tree.ParentId,
            Alias = tree.Alias ?? string.Empty,
            CaseSensitive = tree.CaseSensitive ? 0 : 1,
            Component = tree.Component,
            Meta = JsonSerializer.Serialize(tree.Meta),
            Name = tree.Name,
            Path = tree.Path,
            Redirect = tree.Redirect ?? string.Empty
        };

        _viewResourceRepository.AddViewResource(entity);
        if (tree?.Children?.Count > 0 && entity.Id > 0)
        {
            foreach (var child in tree.Children)
            {
                child.ParentId = entity.Id;
                AddTree(child);
            }
        }
    }
    public void ImportViewTree(List<ViewMenuTree> model)
    {
        foreach (var tree in model)
        {
            AddTree(tree);
        }
    }


    public List<SysViewResourcePrem> GetViewResourcePremList(int viewId)
    {
        return _viewResourceRepository.GetViewResourcePremList(viewId);
    }

    public bool RemoveViewResource([Required] int[] ids)
    {
        return _viewResourceRepository.RemoveViewResource(ids);
    }

    public bool RemoveViewResourcePrem([Required] int[] ids)
    {
        return _viewResourceRepository.RemoveViewResourcePrem(ids);
    }

    public bool UpdateViewResource(SysViewResource model)
    {
        return _viewResourceRepository.UpdateViewResource(model);
    }

    public bool UpdateViewResourcePrem(SysViewResourcePrem model)
    {
        return _viewResourceRepository.UpdateViewResourcePrem(model);
    }

    public List<ViewPremTree> GetViewPremTree()
    {
        var views = _viewResourceRepository.GetViewResourceList();
        var prems = _viewResourceRepository.GetViewResourcePremList();

        List<ViewPremTree> list = new();

        foreach (var view in views)
        {
            MetaConvert meta = new(view.Meta);
            list.Add(new ViewPremTree
            {
                Id = view.Id,
                ParentId = view.ParentId,
                Title = meta.Title,
                Icon = meta.Icon
            });
        }
        foreach (var prem in prems)
        {
            list.Add(new ViewPremTree
            {
                Id = 0 - prem.Id,
                ParentId = prem.ViewId,
                Icon = string.Empty,
                Title = prem.PremName
            });
        }

        return TreeBuilder.Build(list);
    }
}
