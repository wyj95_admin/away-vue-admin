﻿namespace Away.Admin.Services.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class AdminService : IAdminService
{
    private readonly IJwtTokenBuilder _jwtTokenBuilder;
    private readonly IAdminRepository _adminRepository;
    public AdminService(IAdminRepository sysAdminRepository, IJwtTokenBuilder jwtTokenBuilder)
    {
        _adminRepository = sysAdminRepository;
        _jwtTokenBuilder = jwtTokenBuilder;
    }

    public bool AddAdmin(Register model)
    {
        var salt = Guid.NewGuid().ToString("N")[..8];
        var password = model.PasswordMd5(salt);
        return _adminRepository.AddAdmin(new SysAdmin
        {
            Nickname = model.Username,
            Username = model.Username,
            Password = password,
            Salt = salt,
        });
    }

    public ApiResult Login(Login dto)
    {
        var admin = _adminRepository.GetAdminByUserName(dto.Username);
        if (admin == null || admin.Password != dto.PasswordMd5(admin.Salt))
        {
            return ApiResult.Ok(1, "账号或密码错误");
        }
        var data = _jwtTokenBuilder.Bulder(admin.Id);
        return ApiResult.Ok(data);
    }

    public bool Logout(int uid)
    {
        return true;
    }

    public ApiResult GetAdminPage(AdminSearch search)
    {
        return _adminRepository.GetAdminPage(search);
    }

    public bool RemoveAdmin([Required] int[] ids)
    {
        return _adminRepository.RemoveAdmin(ids);
    }

    public bool UpdateAdmin(SysAdmin model)
    {
        return _adminRepository.UpdateAdmin(model);
    }

    public bool SetApiResource(Permission model)
    {
        model.Mid = model.Mid.Where(o => o > 0).ToList();
        return _adminRepository.SetApiResource(model);
    }

    public bool SetOrganization(Permission model)
    {
        return _adminRepository.SetOrganization(model);
    }

    public bool SetRole(Permission model)
    {
        return _adminRepository.SetRoot(model);
    }

    public bool SetViewResource(Permission model)
    {
        return _adminRepository.SetViewResource(model);
    }

    public AdminInfo GetAdminInfo(int uid)
    {
        return _adminRepository.GetAdminInfo(uid);
    }

    public bool ExistApiPermission(ApiPermSearch search)
    {
        return _adminRepository.HasApiPerm(search);
    }

    public List<string> GetViewPermCode(int uid)
    {
        return _adminRepository.GetViewPermCode(uid);
    }

    public List<ViewMenuTree> GetViewTree(int uid)
    {
        var list = _adminRepository.GetViews(uid);
        var items = list.Select(o => new ViewMenuTree
        {
            Id = o.Id,
            ParentId = o.ParentId,
            Alias = o.Alias,
            CaseSensitive = o.CaseSensitive == 0,
            Component = o.Component,
            Meta = JsonSerializer.Deserialize<Dictionary<string, object>>(o.Meta)!,
            Name = o.Name,
            Path = o.Path,
            Redirect = o.Redirect
        }).ToList();
        return TreeBuilder.Build(items);
    }
}
