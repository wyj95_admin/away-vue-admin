﻿namespace Away.Admin.Services.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class RoleService : IRoleService
{
    private readonly IRoleRepository _roleRepository;
    public RoleService(IRoleRepository rootRepository)
    {
        _roleRepository = rootRepository;
    }

    public bool AddRole(SysRole model)
    {
        return _roleRepository.AddRole(model);
    }

    public ApiResult GetRolePage(RoleSearch search)
    {
        return _roleRepository.GetRolePage(search);
    }

    public bool RemoveRole([Required] int[] ids)
    {
        return _roleRepository.RemoveRole(ids);
    }

    public bool UpdateRole(SysRole model)
    {
        return _roleRepository.UpdateRole(model);
    }

    public bool SetApiResource(Permission model)
    {
        return _roleRepository.SetApiResource(model);
    }

    public bool SetViewResource(Permission model)
    {
        return _roleRepository.SetViewResource(model);
    }

    public List<RoleTree> GetRoleTree()
    {
        var list = _roleRepository.GetRoleList();
        var tree = list.Select(o => new RoleTree
        {
            Id = o.Id,
            Name = o.RoleName,
            Disabled = o.Status != 0
        }).ToList();
        return TreeBuilder.Build(tree);
    }
}
