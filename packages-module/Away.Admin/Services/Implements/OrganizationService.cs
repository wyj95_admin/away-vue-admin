﻿namespace Away.Admin.Services.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class OrganizationService : IOrganizationService
{
    private readonly IOrganizationRepository _organizationRepository;
    public OrganizationService(IOrganizationRepository organizationRepository)
    {
        _organizationRepository = organizationRepository;
    }

    public bool AddOrganization(SysOrganization model)
    {
        return _organizationRepository.AddOrganization(model);
    }

    public List<OrganizationTree> GetOrganizationTree(OrganizationSearch search)
    {
        var list = _organizationRepository.GetOrganizationList(search);
        var items = list.Select(o => new OrganizationTree
        {
            Id = o.Id,
            ParentId = o.ParentId,
            CreateTime = o.CreateTime,
            DeptName = o.DeptName,
            OrderNo = o.OrderNo,
            Remark = o.Remark,
            Status = o.Status,
        }).ToList();

        if (search.Status.HasValue || !string.IsNullOrWhiteSpace(search.DeptName))
        {
            return items;
        }
        return TreeBuilder.Build(items);
    }

    public bool RemoveOrganization([Required] int[] ids)
    {
        return _organizationRepository.RemoveOrganization(ids);
    }

    public bool UpdateOrganization(SysOrganization model)
    {
        return _organizationRepository.UpdateOrganization(model);
    }
}
