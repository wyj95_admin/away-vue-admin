﻿namespace Away.Admin.Services;

/// <summary>
/// 前端页面资源服务
/// </summary>
public interface IViewResourceService
{
    /// <summary>
    /// 添加前端页面资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddViewResource(SysViewResource model);
    /// <summary>
    /// 修改前端页面资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateViewResource(SysViewResource model);
    /// <summary>
    /// 删除前端页面资源
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveViewResource([Required] int[] ids);
    /// <summary>
    /// 获取前端页面资源树形列表
    /// </summary>
    /// <returns></returns>
    List<ViewResourceTree> GetViewResourceTree();
    /// <summary>
    /// 查询前端资源+前端权限码列表
    /// </summary>
    /// <returns></returns>
    List<ViewPremTree> GetViewPremTree();
    /// <summary>
    /// 导入前端页面资源
    /// </summary>
    /// <param name="model"></param>
    void ImportViewTree(List<ViewMenuTree> model);
    /// <summary>
    /// 添加前端页面操作权限码
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddViewResourcePrem(SysViewResourcePrem model);
    /// <summary>
    ///  修改前端页面操作权限码
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateViewResourcePrem(SysViewResourcePrem model);
    /// <summary>
    /// 删除前端页面操作权限码
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveViewResourcePrem([Required] int[] ids);
    /// <summary>
    /// 获取前端页面操作权限码列表
    /// </summary>
    /// <param name="viewId"></param>
    /// <returns></returns>
    List<SysViewResourcePrem> GetViewResourcePremList(int viewId);
}
