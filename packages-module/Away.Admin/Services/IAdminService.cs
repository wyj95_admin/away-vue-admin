﻿namespace Away.Admin.Services;

/// <summary>
/// 管理员服务
/// </summary>
public interface IAdminService
{
    /// <summary>
    /// 添加管理员
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddAdmin(Register model);
    /// <summary>
    /// 修改管理员
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateAdmin(SysAdmin model);
    /// <summary>
    /// 删除管理员
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveAdmin([Required] int[] ids);
    ApiResult GetAdminPage(AdminSearch search);
    /// <summary>
    /// 配置接口权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetApiResource(Permission model);
    /// <summary>
    /// 配置前端权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetViewResource(Permission model);
    /// <summary>
    /// 配置角色
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetRole(Permission model);
    /// <summary>
    /// 配置组织架构
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetOrganization(Permission model);
    ApiResult Login(Login dto);
    bool Logout(int uid);
    /// <summary>
    /// 获取管理员信息
    /// </summary>
    /// <param name="uid"></param>
    AdminInfo GetAdminInfo(int uid);
    /// <summary>
    /// 获取前端权限码
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    List<string> GetViewPermCode(int uid);
    /// <summary>
    /// 获取前端菜单
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    List<ViewMenuTree> GetViewTree(int uid);
    /// <summary>
    /// 是否有接口权限
    /// </summary>
    /// <returns></returns>
    bool ExistApiPermission(ApiPermSearch search);
}
