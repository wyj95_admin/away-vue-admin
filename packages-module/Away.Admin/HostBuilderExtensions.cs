﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Away.Admin;

public static class HostBuilderExtensions
{
    /// <summary>
    /// 添加系统管理员权限模块
    /// </summary>
    /// <param name="builder"></param>
    public static void AddSystems(this IHostBuilder builder)
    {
        builder.ConfigureServices((host, services) =>
        {            
            var conn = host.Configuration.GetConnectionString("MySql");
            var ver = ServerVersion.Parse("8.0.26");
            services.AddDbContext<SystemsDbContext>(o => o.UseMySql(conn, ver));
            services.AddHostedService<InitTablesHostService<SystemsDbContext>>();
        });
    }
}
