﻿namespace Away.Admin.Controllers;

/// <summary>
/// 前端资源接口
/// </summary>
[Route("sys/view_resource")]
public class SysViewResourceController : ApiControllerBase
{
    private readonly IViewResourceService _viewResourceService;
    public SysViewResourceController(IViewResourceService viewResourceService)
    {
        _viewResourceService = viewResourceService;
    }

    /// <summary>
    /// 导入前端资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("import")]
    public IActionResult ImportViewTree(List<ViewMenuTree> model)
    {
        _viewResourceService.ImportViewTree(model);
        return ApiOk();
    }

    /// <summary>
    /// 添加前端资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult AddViewResource(SysViewResource model)
    {
        var data = _viewResourceService.AddViewResource(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 修改前端资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut]
    public IActionResult UpdateViewResource(SysViewResource model)
    {
        var data = _viewResourceService.UpdateViewResource(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 删除前端资源
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete]
    public IActionResult RemoveViewResource([Required] int[] ids)
    {
        var data = _viewResourceService.RemoveViewResource(ids);
        return ApiOk(data);
    }

    /// <summary>
    /// 前端资源列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("tree")]
    public IActionResult GetViewResourceTree()
    {
        var data = _viewResourceService.GetViewResourceTree();
        return ApiOk(data);
    }
    /// <summary>
    /// 查询前端资源+前端权限码列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("and_prem/tree")]
    public IActionResult GetViewPremTree()
    {
        var data = _viewResourceService.GetViewPremTree();
        return ApiOk(data);
    }

    /// <summary>
    /// 添加权限码
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("prem_code")]
    public IActionResult AddViewResourcePrem(SysViewResourcePrem model)
    {
        var data = _viewResourceService.AddViewResourcePrem(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 修改权限码
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut("prem_code")]
    public IActionResult UpdateViewResourcePrem(SysViewResourcePrem model)
    {
        var data = _viewResourceService.UpdateViewResourcePrem(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 删除权限码
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete("prem_code")]
    public IActionResult RemoveViewResourcePrem([Required] int[] ids)
    {
        var data = _viewResourceService.RemoveViewResourcePrem(ids);
        return ApiOk(data);
    }

    /// <summary>
    /// 根据前端页面获取权限码
    /// </summary>
    /// <param name="viewId"></param>
    /// <returns></returns>
    [HttpGet("prem_code")]
    public IActionResult GetViewResourcePremList(int viewId)
    {
        var data = _viewResourceService.GetViewResourcePremList(viewId);
        return ApiOk(data);
    }
}
