﻿namespace Away.Admin.Controllers;

[ApiController, JwtToken, Auth, ExceptionLogger, RequestRawLogger, ValidatResult]
public abstract class ApiControllerBase : ApiController
{
    protected int AdminId => GetUserId();
}