﻿namespace Away.Admin.Controllers;

/// <summary>
/// 接口管理
/// </summary>
[Route("sys/api_resource")]
public class SysApiResourceController : ApiControllerBase
{
    private readonly IApiResourceService _apiResourceService;
    public SysApiResourceController(IApiResourceService apiResourceService)
    {
        _apiResourceService = apiResourceService;
    }

    /// <summary>
    /// 导入swagger接口
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    [HttpPost("import_swagger")]
    public async Task<IActionResult> ImportSwaggerApi([FromBody] string url)
    {
        await _apiResourceService.ImportSwaggerApi(url);
        return ApiOk();
    }

    /// <summary>
    /// 获取接口树形列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("tree")]
    public IActionResult GetApiResourceTree()
    {
        var data = _apiResourceService.GetApiResourceTree();
        return ApiOk(data);
    }

    /// <summary>
    /// 添加API资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult AddApiResource(SysApiResource model)
    {
        var data = _apiResourceService.AddApiResource(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 更新API资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut]
    public IActionResult UpdateApiResource(SysApiResource model)
    {
        var data = _apiResourceService.UpdateApiResource(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 删除API资源
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete]
    public IActionResult RemoveApiResource([Required] int[] ids)
    {
        var data = _apiResourceService.RemoveApiResource(ids);
        return ApiOk(data);
    }

    /// <summary>
    /// API资源分页
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    [HttpGet]
    public IActionResult PageApiResource([FromQuery] ApiResourceSearch search)
    {
        return _apiResourceService.GetApiResourcePage(search);
    }
}
