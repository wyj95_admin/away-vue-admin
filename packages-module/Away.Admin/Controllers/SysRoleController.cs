﻿namespace Away.Admin.Controllers;

/// <summary>
/// 角色接口
/// </summary>
[Route("sys/role")]
public class SysRoleController : ApiControllerBase
{
    private readonly IRoleService _roleService;
    public SysRoleController(IRoleService rootService)
    {
        _roleService = rootService;
    }

    /// <summary>
    /// 添加角色
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult AddRole(SysRole model)
    {
        var data = _roleService.AddRole(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 更新角色
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut]
    public IActionResult UpdateRole(SysRole model)
    {
        var data = _roleService.UpdateRole(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 删除角色
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete]
    public IActionResult DeleteRole([Required] int[] ids)
    {
        var data = _roleService.RemoveRole(ids);
        return ApiOk(data);
    }

    /// <summary>
    /// 角色分页
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    [HttpGet]
    public IActionResult GetRolePage([FromQuery] RoleSearch search)
    {
        return _roleService.GetRolePage(search);
    }

    /// <summary>
    /// 获取角色树形列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("tree")]
    public IActionResult GetRoleTree()
    {
        var data = _roleService.GetRoleTree();
        return ApiOk(data);
    }

    /// <summary>
    /// 角色授权前端权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("permission/view_resource")]
    public IActionResult SetViewResource(Permission model)
    {
        var data = _roleService.SetViewResource(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 角色授权API权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("permission/api_resource")]
    public IActionResult SetApiResource(Permission model)
    {
        var data = _roleService.SetApiResource(model);
        return ApiOk(data);
    }

}
