﻿namespace Away.Admin.Controllers;

/// <summary>
/// 管理员接口
/// </summary>
[Route("sys/admin")]
public class SysAdminController : ApiControllerBase
{
    private readonly IAdminService _adminService;
    public SysAdminController(IAdminService adminService)
    {
        _adminService = adminService;
    }

    /// <summary>
    /// 获取管理员信息
    /// </summary>
    /// <returns></returns>
    [HttpGet("info")]
    public IActionResult GetAdminInfo()
    {
        var data = _adminService.GetAdminInfo(AdminId);
        return ApiOk(data);
    }

    /// <summary>
    /// 获取前端权限码
    /// </summary>
    /// <returns></returns>
    [HttpGet("perm_code")]
    public IActionResult GetViewPermCode()
    {
        var data = _adminService.GetViewPermCode(AdminId);
        return ApiOk(data);
    }

    /// <summary>
    /// 获取前端菜单
    /// </summary>
    /// <returns></returns>
    [HttpGet("views")]
    public IActionResult GetViewTree()
    {
        var data = _adminService.GetViewTree(AdminId);
        return ApiOk(data);
    }

    /// <summary>
    /// 管理员登录
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("login"), Allow]
    public IActionResult Login(Login model)
    {
        var data = _adminService.Login(model);
        return data;
    }

    /// <summary>
    /// 管理员登出
    /// </summary>
    /// <returns></returns>
    [HttpGet("logout")]
    public IActionResult Logout()
    {
        var data = _adminService.Logout(AdminId);
        return ApiOk(data);
    }

    /// <summary>
    /// 添加管理员
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult AddAdmin(Register model)
    {
        var data = _adminService.AddAdmin(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 更新管理员
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut]
    public IActionResult UpdateAdmin(SysAdmin model)
    {
        var data = _adminService.UpdateAdmin(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 删除管理员
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete]
    public IActionResult RemoveAdmin([Required] int[] ids)
    {
        var data = _adminService.RemoveAdmin(ids);
        return ApiOk(data);
    }

    /// <summary>
    /// 管理员分页
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    [HttpGet]
    public IActionResult GetAdminPage([FromQuery] AdminSearch search)
    {
        return _adminService.GetAdminPage(search);
    }

    /// <summary>
    /// 管理员授权前端权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("permission/view_resource")]
    public IActionResult SetViewResource(Permission model)
    {
        var data = _adminService.SetViewResource(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 管理员授权API权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("permission/api_resource")]
    public IActionResult SetApiResource(Permission model)
    {
        var data = _adminService.SetApiResource(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 管理员分配组织架构
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("permission/organization")]
    public IActionResult SetOrganization(Permission model)
    {
        var data = _adminService.SetOrganization(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 管理员授权角色
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("permission/role")]
    public IActionResult SetRole(Permission model)
    {
        var data = _adminService.SetRole(model);
        return ApiOk(data);
    }

}
