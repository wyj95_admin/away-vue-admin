﻿namespace Away.Admin.Controllers;

/// <summary>
/// 组织架构接口
/// </summary>
[Route("sys/organization")]
public class SysOrganizationController : ApiControllerBase
{
    private readonly IOrganizationService _organizationService;
    public SysOrganizationController(IOrganizationService organizationService)
    {
        _organizationService = organizationService;
    }

    /// <summary>
    /// 添加组织架构
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult AddOrganization(SysOrganization model)
    {
        var data = _organizationService.AddOrganization(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 更新组织架构
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut]
    public IActionResult UpdateOrganization(SysOrganization model)
    {
        var data = _organizationService.UpdateOrganization(model);
        return ApiOk(data);
    }

    /// <summary>
    /// 删除组织架构
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete]
    public IActionResult RemoveOrganization([Required] int[] ids)
    {
        var data = _organizationService.RemoveOrganization(ids);
        return ApiOk(data);
    }

    /// <summary>
    /// 获取组织架构树形列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("tree")]
    public IActionResult GetOrganizationTree([FromQuery] OrganizationSearch search)
    {
        var data = _organizationService.GetOrganizationTree(search);
        return ApiOk(data);
    }
}
