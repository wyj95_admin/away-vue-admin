﻿namespace Away.Admin.Repositories.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class ViewResourceRepository : SystemRepository, IViewResourceRepository
{
    public ViewResourceRepository(SystemsDbContext context) : base(context)
    {
    }

    public bool AddViewResource(SysViewResource model)
    {
        Add(model);
        return Save();
    }

    public bool AddViewResourcePrem(SysViewResourcePrem model)
    {
        Add(model);
        return Save();
    }

    public List<SysViewResource> GetViewResourceList()
    {
        return Query<SysViewResource>().ToList();
    }

    public List<SysViewResourcePrem> GetViewResourcePremList(int? viewId)
    {
        var cond = CondBuilder.New<SysViewResourcePrem>(true)
            .And(viewId.HasValue, o => o.ViewId == viewId);
        return Query<SysViewResourcePrem>().Where(cond).ToList();
    }

    public bool RemoveViewResource([Required] int[] ids)
    {
        var items = Query<SysViewResource>().Where(o => ids.Contains(o.Id)).ToArray();
        if (!items.Any())
        {
            return true;
        }
        Remove(items);
        return Save();
    }

    public bool RemoveViewResourcePrem([Required] int[] ids)
    {
        var items = Query<SysViewResourcePrem>().Where(o => ids.Contains(o.Id)).ToArray();
        if (!items.Any())
        {
            return true;
        }
        Remove(items);
        return Save();
    }

    public bool UpdateViewResource(SysViewResource model)
    {
        Update(model);
        return Save();
    }

    public bool UpdateViewResourcePrem(SysViewResourcePrem model)
    {
        Update(model);
        return Save();
    }
}
