﻿namespace Away.Admin.Repositories.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class RoleRepository : SystemRepository, IRoleRepository
{
    public RoleRepository(SystemsDbContext context) : base(context)
    {
    }

    public bool AddRole(SysRole model)
    {
        Add(model);
        return Save();
    }

    public bool UpdateRole(SysRole model)
    {
        Update(model);
        return Save();
    }

    public bool RemoveRole([Required] int[] ids)
    {
        var items = Query<SysRole>().Where(o => ids.Contains(o.Id)).ToArray();
        if (!items.Any())
        {
            return true;
        }
        Remove(items);
        return Save();
    }

    public ApiResult GetRolePage(RoleSearch search)
    {
        var cond = CondBuilder.New<SysRole>(true)
            .And(search.Id > 0, o => o.Id == search.Id)
            .And(search.Status.HasValue, o => o.Status == search.Status)
            .And(!string.IsNullOrWhiteSpace(search.RoleName), o => o.RoleName.Contains(search.RoleName))
            .And(!string.IsNullOrWhiteSpace(search.RoleValue), o => o.RoleValue.Contains(search.RoleValue));

        var tb = from role in Query<SysRole>().Where(cond)
                 select new
                 {
                     role.OrderNo,
                     role.RoleName,
                     role.RoleValue,
                     role.Status,
                     role.CreateTime,
                     role.Remark,
                     role.Id,
                     apis = (from api in Query<SysRoleApiResource>() where api.Oid == role.Id select api.Mid).ToArray(),
                     views = (from view in Query<SysRoleViewResource>().Where(o => o.Oid == role.Id) select view.Mid).ToArray()
                 };

        var total = tb.Count();
        var items = tb.OrderByDescending(o => o.Id).Page(search).ToList();
        return ApiResult.Page(total, items);
    }

    public bool SetApiResource(Permission model)
    {
        var items = Query<SysRoleApiResource>().Where(o => o.Oid == model.Oid).ToList();
        var dels = items.Where(o => !model.Mid.Contains(o.Mid)).ToArray();
        if (dels.Any())
        {
            Remove(dels);
        }
        var adds = model.Mid
            .Where(o => !items.Any(t => t.Mid == o))
            .Select(o => new SysRoleApiResource { Mid = o, Oid = model.Oid })
            .ToArray();
        if (adds.Any())
        {
            Add(adds);
        }
        return Save();
    }

    public bool SetViewResource(Permission model)
    {
        var items = Query<SysRoleViewResource>().Where(o => o.Oid == model.Oid).ToList();
        var dels = items.Where(o => !model.Mid.Contains(o.Mid)).ToArray();
        if (dels.Any())
        {
            Remove(dels);
        }
        var adds = model.Mid
            .Where(o => !items.Any(t => t.Mid == o))
            .Select(o => new SysRoleViewResource { Mid = o, Oid = model.Oid })
            .ToArray();
        if (adds.Any())
        {
            Add(adds);
        }
        return Save();
    }

    public List<SysRole> GetRoleList()
    {
        return Query<SysRole>().ToList();
    }
}
