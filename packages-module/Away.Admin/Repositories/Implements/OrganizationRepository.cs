﻿namespace Away.Admin.Repositories.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class OrganizationRepository : SystemRepository, IOrganizationRepository
{
    public OrganizationRepository(SystemsDbContext context) : base(context)
    {
    }

    public bool AddOrganization(SysOrganization model)
    {
        Add(model);
        return Save();
    }

    public List<SysOrganization> GetOrganizationList(OrganizationSearch search)
    {
        var cond = CondBuilder.New<SysOrganization>(true)
            .And(!string.IsNullOrWhiteSpace(search.DeptName), o => o.DeptName.Contains(search.DeptName))
            .And(search.Status.HasValue, o => o.Status == search.Status);
        return Query<SysOrganization>().Where(cond).ToList();
    }

    public bool RemoveOrganization([Required] int[] ids)
    {
        var items = Query<SysOrganization>().Where(o => ids.Contains(o.Id)).ToArray();
        if (!items.Any())
        {
            return true;
        }
        Remove(items);
        return Save();
    }

    public bool UpdateOrganization(SysOrganization model)
    {
        Update(model);
        return Save();
    }
}
