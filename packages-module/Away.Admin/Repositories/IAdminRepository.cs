﻿namespace Away.Admin.Repositories;

/// <summary>
/// 管理员仓储
/// </summary>
public interface IAdminRepository
{
    /// <summary>
    /// 添加管理员
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddAdmin(SysAdmin model);
    /// <summary>
    /// 修改管理员
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateAdmin(SysAdmin model);
    /// <summary>
    /// 删除管理员
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveAdmin([Required] int[] ids);
    /// <summary>
    /// 获取管理员分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetAdminPage(AdminSearch search);
    /// <summary>
    /// 配置接口权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetApiResource(Permission model);
    /// <summary>
    /// 配置前端权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetViewResource(Permission model);
    /// <summary>
    /// 配置角色
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetRoot(Permission model);
    /// <summary>
    /// 配置组织架构
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetOrganization(Permission model);
    /// <summary>
    /// 根据用户名查询管理员
    /// </summary>
    /// <param name="userName"></param>
    /// <returns></returns>
    SysAdmin? GetAdminByUserName(string userName);
    /// <summary>
    /// 根据用户编号查询管理员
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    AdminInfo GetAdminInfo(int uid);
    /// <summary>
    /// 获取前端权限码
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    List<string> GetViewPermCode(int uid);
    /// <summary>
    /// 获取前端菜单
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    List<SysViewResource> GetViews(int uid);
    /// <summary>
    /// 是否有接口权限
    /// </summary>
    /// <returns></returns>
    bool HasApiPerm(ApiPermSearch search);
}
