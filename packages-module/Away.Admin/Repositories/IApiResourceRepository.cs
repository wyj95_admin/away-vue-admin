﻿namespace Away.Admin.Repositories;

/// <summary>
/// 接口资源仓储
/// </summary>
public interface IApiResourceRepository
{
    /// <summary>
    /// 添加接口资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddApiResource(SysApiResource model);
    /// <summary>
    /// 更新接口资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateApiResource(SysApiResource model);
    /// <summary>
    /// 删除接口资源
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveApiResource([Required] int[] ids);
    /// <summary>
    /// 获取接口资源分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetApiResourcePage(ApiResourceSearch search);
    /// <summary>
    /// 导入接口资源数据
    /// </summary>
    /// <param name="entitys"></param>
    /// <returns></returns>
    void ImportApiResource(List<SysApiResource> entitys);
    /// <summary>
    /// 获取接口资源列表
    /// </summary>
    /// <returns></returns>
    List<SysApiResource> GetApiResourceList();
}
