﻿namespace Away.Admin.Repositories;

/// <summary>
/// 角色仓储
/// </summary>
public interface IRoleRepository
{
    /// <summary>
    /// 添加角色
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddRole(SysRole model);
    /// <summary>
    /// 更新角色
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateRole(SysRole model);
    /// <summary>
    /// 删除角色
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveRole([Required] int[] ids);
    /// <summary>
    /// 获取角色分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetRolePage(RoleSearch search);
    /// <summary>
    /// 获取角色列表
    /// </summary>
    /// <returns></returns>
    List<SysRole> GetRoleList();
    /// <summary>
    /// 配置接口权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetApiResource(Permission model);
    /// <summary>
    /// 配置前端权限
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool SetViewResource(Permission model);
}
