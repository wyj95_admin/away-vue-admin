﻿namespace Away.Admin.Repositories;

public abstract class SystemRepository : RepositoryBase<SystemsDbContext>
{
    protected SystemRepository(SystemsDbContext context) : base(context)
    {

    }
}
