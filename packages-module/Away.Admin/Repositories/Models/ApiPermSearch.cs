﻿namespace Away.Admin.Repositories.Models;

public class ApiPermSearch
{
    public int AdminId { get; set; }
    public required string Method { get; set; }
    public required string Url { get; set; }
}
