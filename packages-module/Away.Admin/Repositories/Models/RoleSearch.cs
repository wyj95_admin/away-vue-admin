﻿namespace Away.Admin.Repositories.Models;

public class RoleSearch : BasePage
{
    private string _rolename = string.Empty;
    private string _rolevalue = string.Empty;

    public int Id { get; set; }
    /// <summary>
    /// 状态:0启用；1禁用
    /// </summary>
    public int? Status { get; set; }
    public string RoleName
    {
        get => string.IsNullOrWhiteSpace(_rolename) ? string.Empty : _rolename.Trim();
        set => _rolename = value;
    }
    public string RoleValue
    {
        get => string.IsNullOrWhiteSpace(_rolevalue) ? string.Empty : _rolevalue.Trim();
        set => _rolevalue = value;
    }
}
