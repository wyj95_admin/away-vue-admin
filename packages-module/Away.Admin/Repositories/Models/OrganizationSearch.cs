﻿namespace Away.Admin.Repositories.Models;

public class OrganizationSearch
{
    private string _deptName = string.Empty;

    /// <summary>
    /// 部门名称
    /// </summary>
    public string DeptName
    {
        get => string.IsNullOrWhiteSpace(_deptName) ? string.Empty : _deptName.Trim();
        set => _deptName = value;
    }

    /// <summary>
    /// 状态：0启用；1禁用
    /// </summary>
    public int? Status { get; set; }
}
