﻿namespace Away.Admin.Repositories.Models;

public class Permission
{
    public int Oid { get; set; }

    public required List<int> Mid { get; set; }
}
