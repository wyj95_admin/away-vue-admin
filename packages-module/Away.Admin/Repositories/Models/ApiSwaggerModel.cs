﻿namespace Away.Admin.Repositories.Models;

public class ApiSwaggerModel
{
    public required Dictionary<string, Dictionary<string, SwaggerTag>> Paths { get; set; }

    public List<SysApiResource> ToEntities()
    {
        List<SysApiResource> _list = new();
        foreach (var (url, methods) in Paths)
        {
            foreach (var (method, info) in methods)
            {
                var entity = new SysApiResource
                {
                    ApiName = info.Summary,
                    Group = info.Tags.FirstOrDefault() ?? string.Empty,
                    Method = method,
                    Url = url,
                };
                _list.Add(entity);
            }
        }
        return _list;
    }
}

public class SwaggerTag
{
    public required string Summary { get; set; }
    public required string[] Tags { get; set; }
}