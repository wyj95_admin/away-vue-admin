﻿namespace Away.Admin.Repositories.Models;

public class AdminInfo
{
    public List<RootInfo>? Roots { get; set; }
    public List<string>? Orgs { get; set; }

    /// <summary>
    /// 管理员编号
    /// </summary>
    public int UserId { get; set; }

    /// <summary>
    /// 用户名
    /// </summary>
    public string Username { get; set; } = null!;

    /// <summary>
    /// 真实姓名
    /// </summary>
    public string Realname { get; set; } = null!;

    /// <summary>
    /// 头像
    /// </summary>
    public string Avatar { get; set; } = null!;

    /// <summary>
    /// 简介
    /// </summary>
    public string Desc { get; set; } = null!;
}

public class RootInfo
{
    public required string RoleName { get; set; }
    public required string RoleValue { get; set; }
}
