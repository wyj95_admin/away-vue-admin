﻿namespace Away.Admin.Repositories.Models;

public class AdminSearch : BasePage
{
    /// <summary>
    /// 管理员编号
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    public string? Nickname { get; set; }

    /// <summary>
    /// 用户名
    /// </summary>
    public string? Username { get; set; }

    /// <summary>
    /// 真实姓名
    /// </summary>
    public string? Realname { get; set; }

    /// <summary>
    /// 部门编号
    /// </summary>
    public int OrgId { get; set; }
}
