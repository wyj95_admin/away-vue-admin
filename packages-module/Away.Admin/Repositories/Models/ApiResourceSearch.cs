﻿namespace Away.Admin.Repositories.Models;

public class ApiResourceSearch : BasePage
{
    public int Id { get; set; }

    /// <summary>
    /// 接口URL
    /// </summary>
    public string? Url { get; set; }

    /// <summary>
    /// 接口名称
    /// </summary>
    public string? ApiName { get; set; }
    public string? Group { get; set; }
}
