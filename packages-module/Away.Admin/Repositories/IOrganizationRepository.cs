﻿namespace Away.Admin.Repositories;

/// <summary>
/// 组织架构仓储
/// </summary>
public interface IOrganizationRepository
{
    /// <summary>
    /// 添加部门
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddOrganization(SysOrganization model);
    /// <summary>
    /// 更新部门
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateOrganization(SysOrganization model);
    /// <summary>
    /// 删除部门
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveOrganization([Required] int[] ids);
    /// <summary>
    /// 获取部门列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    List<SysOrganization> GetOrganizationList(OrganizationSearch search);
}
