﻿namespace Away.Admin.Repositories;

/// <summary>
/// 前端资源仓储
/// </summary>
public interface IViewResourceRepository
{
    /// <summary>
    /// 添加前端资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddViewResource(SysViewResource model);
    /// <summary>
    /// 修改前端资源
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateViewResource(SysViewResource model);
    /// <summary>
    /// 删除前端资源
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveViewResource([Required] int[] ids);
    /// <summary>
    /// 获取前端资源列表
    /// </summary>
    /// <returns></returns>
    List<SysViewResource> GetViewResourceList();
    /// <summary>
    /// 添加前端权限码
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddViewResourcePrem(SysViewResourcePrem model);
    /// <summary>
    /// 更新前端权限码
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateViewResourcePrem(SysViewResourcePrem model);
    /// <summary>
    /// 删除前端权限码
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool RemoveViewResourcePrem([Required] int[] ids);
    /// <summary>
    /// 获取前端权限码列表
    /// </summary>
    /// <param name="viewId"></param>
    /// <returns></returns>
    List<SysViewResourcePrem> GetViewResourcePremList(int? viewId = null);
}
