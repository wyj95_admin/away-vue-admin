﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Away.Admin.DB;

public partial class SystemsDbContext : DbContext
{
    public SystemsDbContext(DbContextOptions<SystemsDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<SysAdmin> SysAdmins { get; set; }

    public virtual DbSet<SysAdminApiResource> SysAdminApiResources { get; set; }

    public virtual DbSet<SysAdminOrganization> SysAdminOrganizations { get; set; }

    public virtual DbSet<SysAdminRole> SysAdminRoles { get; set; }

    public virtual DbSet<SysAdminViewResource> SysAdminViewResources { get; set; }

    public virtual DbSet<SysApiResource> SysApiResources { get; set; }

    public virtual DbSet<SysOrganization> SysOrganizations { get; set; }

    public virtual DbSet<SysRole> SysRoles { get; set; }

    public virtual DbSet<SysRoleApiResource> SysRoleApiResources { get; set; }

    public virtual DbSet<SysRoleViewResource> SysRoleViewResources { get; set; }

    public virtual DbSet<SysViewResource> SysViewResources { get; set; }

    public virtual DbSet<SysViewResourcePrem> SysViewResourcePrems { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb4_0900_ai_ci")
            .HasCharSet("utf8mb4");

        modelBuilder.Entity<SysAdmin>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_admin", tb => tb.HasComment("管理员表"))
                .UseCollation("utf8mb4_general_ci");

            entity.HasIndex(e => e.Username, "username").IsUnique();

            entity.Property(e => e.Id)
                .HasComment("管理员编号")
                .HasColumnName("id");
            entity.Property(e => e.Avatar)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("头像")
                .HasColumnName("avatar");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.Desc)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("简介")
                .HasColumnName("desc");
            entity.Property(e => e.Nickname)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("昵称")
                .HasColumnName("nickname");
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("密码")
                .HasColumnName("password");
            entity.Property(e => e.Realname)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("真实姓名")
                .HasColumnName("realname");
            entity.Property(e => e.Salt)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("加密盐")
                .HasColumnName("salt");
            entity.Property(e => e.Username)
                .HasDefaultValueSql("''")
                .HasComment("用户名")
                .HasColumnName("username");
        });

        modelBuilder.Entity<SysAdminApiResource>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_admin_api_resource", tb => tb.HasComment("管理员授权API资源表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Mid)
                .HasComment("接口资源编号")
                .HasColumnName("mid");
            entity.Property(e => e.Oid)
                .HasComment("管理员编号")
                .HasColumnName("oid");
        });

        modelBuilder.Entity<SysAdminOrganization>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_admin_organization", tb => tb.HasComment("管理员分配组织架构表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Mid)
                .HasComment("组织架构编号")
                .HasColumnName("mid");
            entity.Property(e => e.Oid)
                .HasComment("管理员编号")
                .HasColumnName("oid");
        });

        modelBuilder.Entity<SysAdminRole>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_admin_role", tb => tb.HasComment("管理员授权角色表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Mid)
                .HasComment("角色编号")
                .HasColumnName("mid");
            entity.Property(e => e.Oid)
                .HasComment("管理员编号")
                .HasColumnName("oid");
        });

        modelBuilder.Entity<SysAdminViewResource>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_admin_view_resource", tb => tb.HasComment("管理员授权前端资源表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Mid)
                .HasComment("前端资源编号")
                .HasColumnName("mid");
            entity.Property(e => e.Oid)
                .HasComment("管理员编号")
                .HasColumnName("oid");
        });

        modelBuilder.Entity<SysApiResource>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_api_resource", tb => tb.HasComment("API资源表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ApiName)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("接口名称")
                .HasColumnName("api_name")
                .UseCollation("utf8mb4_0900_ai_ci");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.Group)
                .HasMaxLength(20)
                .HasDefaultValueSql("''")
                .HasComment("分组")
                .HasColumnName("group");
            entity.Property(e => e.Method)
                .HasMaxLength(10)
                .HasDefaultValueSql("''")
                .HasComment("请求方法")
                .HasColumnName("method");
            entity.Property(e => e.Url)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("接口URL")
                .HasColumnName("url")
                .UseCollation("utf8mb4_0900_ai_ci");
        });

        modelBuilder.Entity<SysOrganization>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_organization", tb => tb.HasComment("组织架构表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id)
                .HasComment("部门编号")
                .HasColumnName("id");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.DeptName)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("部门名称")
                .HasColumnName("dept_name");
            entity.Property(e => e.OrderNo)
                .HasComment("排序")
                .HasColumnName("order_no");
            entity.Property(e => e.ParentId)
                .HasComment("上级编号")
                .HasColumnName("parent_id");
            entity.Property(e => e.Remark)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("备注")
                .HasColumnName("remark");
            entity.Property(e => e.Status)
                .HasComment("状态：0启用；1禁用")
                .HasColumnName("status");
        });

        modelBuilder.Entity<SysRole>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_role", tb => tb.HasComment("角色表"))
                .UseCollation("utf8mb4_general_ci");

            entity.HasIndex(e => e.RoleValue, "role_value").IsUnique();

            entity.Property(e => e.Id)
                .HasComment("角色编号")
                .HasColumnName("id");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.OrderNo)
                .HasComment("排序")
                .HasColumnName("order_no");
            entity.Property(e => e.Remark)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("备注")
                .HasColumnName("remark");
            entity.Property(e => e.RoleName)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("角色名称")
                .HasColumnName("role_name");
            entity.Property(e => e.RoleValue)
                .HasDefaultValueSql("''")
                .HasComment("角色值")
                .HasColumnName("role_value");
            entity.Property(e => e.Status)
                .HasComment("状态:0启用；1禁用")
                .HasColumnName("status");
        });

        modelBuilder.Entity<SysRoleApiResource>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_role_api_resource", tb => tb.HasComment("角色授权API资源表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Mid)
                .HasComment("接口资源编号")
                .HasColumnName("mid");
            entity.Property(e => e.Oid)
                .HasComment("角色编号")
                .HasColumnName("oid");
        });

        modelBuilder.Entity<SysRoleViewResource>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_role_view_resource", tb => tb.HasComment("角色授权前端资源表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Mid)
                .HasComment("前端资源编号")
                .HasColumnName("mid");
            entity.Property(e => e.Oid)
                .HasComment("角色编号")
                .HasColumnName("oid");
        });

        modelBuilder.Entity<SysViewResource>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_view_resource", tb => tb.HasComment("前端资源表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id)
                .HasComment("菜单编号")
                .HasColumnName("id");
            entity.Property(e => e.Alias)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("路由别名")
                .HasColumnName("alias");
            entity.Property(e => e.CaseSensitive).HasColumnName("caseSensitive");
            entity.Property(e => e.Component)
                .HasMaxLength(500)
                .HasDefaultValueSql("''")
                .HasComment("组件路径")
                .HasColumnName("component");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.Meta)
                .HasColumnType("json")
                .HasColumnName("meta");
            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("路由名称")
                .HasColumnName("name");
            entity.Property(e => e.ParentId)
                .HasComment("上级")
                .HasColumnName("parent_id");
            entity.Property(e => e.Path)
                .HasMaxLength(500)
                .HasDefaultValueSql("''")
                .HasComment("路由地址")
                .HasColumnName("path");
            entity.Property(e => e.Redirect)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("跳转地址")
                .HasColumnName("redirect");
            entity.Property(e => e.Status)
                .HasComment("状态:0启用；1禁用")
                .HasColumnName("status");
        });

        modelBuilder.Entity<SysViewResourcePrem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("sys_view_resource_prem", tb => tb.HasComment("前端权限码表"))
                .UseCollation("utf8mb4_general_ci");

            entity.HasIndex(e => e.PremCode, "prem_code").IsUnique();

            entity.Property(e => e.Id)
                .HasComment("前端权限码编号")
                .HasColumnName("id");
            entity.Property(e => e.PremCode)
                .HasDefaultValueSql("''")
                .HasComment("前端权限码")
                .HasColumnName("prem_code");
            entity.Property(e => e.PremName)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("前端权限码名称")
                .HasColumnName("prem_name");
            entity.Property(e => e.ViewId)
                .HasComment("前端资源编号")
                .HasColumnName("view_id");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
