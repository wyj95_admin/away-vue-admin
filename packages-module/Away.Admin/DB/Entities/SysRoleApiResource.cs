﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 角色授权API资源表
/// </summary>
public partial class SysRoleApiResource
{
    /// <summary>
    /// 角色编号
    /// </summary>
    public int Oid { get; set; }

    /// <summary>
    /// 接口资源编号
    /// </summary>
    public int Mid { get; set; }

    public int Id { get; set; }
}
