﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 管理员授权角色表
/// </summary>
public partial class SysAdminRole
{
    /// <summary>
    /// 管理员编号
    /// </summary>
    public int Oid { get; set; }

    /// <summary>
    /// 角色编号
    /// </summary>
    public int Mid { get; set; }

    public int Id { get; set; }
}
