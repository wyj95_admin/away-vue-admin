﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 管理员表
/// </summary>
public partial class SysAdmin
{
    /// <summary>
    /// 管理员编号
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    public string Nickname { get; set; } = null!;

    /// <summary>
    /// 用户名
    /// </summary>
    public string Username { get; set; } = null!;

    /// <summary>
    /// 真实姓名
    /// </summary>
    public string Realname { get; set; } = null!;

    /// <summary>
    /// 头像
    /// </summary>
    public string Avatar { get; set; } = null!;

    /// <summary>
    /// 简介
    /// </summary>
    public string Desc { get; set; } = null!;

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 密码
    /// </summary>
    public string Password { get; set; } = null!;

    /// <summary>
    /// 加密盐
    /// </summary>
    public string Salt { get; set; } = null!;
}
