﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 角色授权前端资源表
/// </summary>
public partial class SysRoleViewResource
{
    /// <summary>
    /// 角色编号
    /// </summary>
    public int Oid { get; set; }

    /// <summary>
    /// 前端资源编号
    /// </summary>
    public int Mid { get; set; }

    public int Id { get; set; }
}
