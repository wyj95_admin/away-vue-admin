﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 角色表
/// </summary>
public partial class SysRole
{
    /// <summary>
    /// 角色编号
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 角色名称
    /// </summary>
    public string RoleName { get; set; } = null!;

    /// <summary>
    /// 角色值
    /// </summary>
    public string RoleValue { get; set; } = null!;

    /// <summary>
    /// 排序
    /// </summary>
    public int OrderNo { get; set; }

    /// <summary>
    /// 状态:0启用；1禁用
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; } = null!;
}
