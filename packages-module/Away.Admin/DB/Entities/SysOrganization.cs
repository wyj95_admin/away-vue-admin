﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 组织架构表
/// </summary>
public partial class SysOrganization
{
    /// <summary>
    /// 部门编号
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 部门名称
    /// </summary>
    public string DeptName { get; set; } = null!;

    /// <summary>
    /// 状态：0启用；1禁用
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// 上级编号
    /// </summary>
    public int ParentId { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int OrderNo { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; } = null!;

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }
}
