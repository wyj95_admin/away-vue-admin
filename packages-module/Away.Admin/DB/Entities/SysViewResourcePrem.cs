﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 前端权限码表
/// </summary>
public partial class SysViewResourcePrem
{
    /// <summary>
    /// 前端权限码编号
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 前端资源编号
    /// </summary>
    public int ViewId { get; set; }

    /// <summary>
    /// 前端权限码
    /// </summary>
    public string PremCode { get; set; } = null!;

    /// <summary>
    /// 前端权限码名称
    /// </summary>
    public string PremName { get; set; } = null!;
}
