﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// API资源表
/// </summary>
public partial class SysApiResource
{
    public int Id { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 接口URL
    /// </summary>
    public string Url { get; set; } = null!;

    /// <summary>
    /// 接口名称
    /// </summary>
    public string ApiName { get; set; } = null!;

    /// <summary>
    /// 请求方法
    /// </summary>
    public string Method { get; set; } = null!;

    /// <summary>
    /// 分组
    /// </summary>
    public string Group { get; set; } = null!;
}
