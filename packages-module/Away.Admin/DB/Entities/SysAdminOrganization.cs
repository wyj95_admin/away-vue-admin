﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 管理员分配组织架构表
/// </summary>
public partial class SysAdminOrganization
{
    /// <summary>
    /// 管理员编号
    /// </summary>
    public int Oid { get; set; }

    /// <summary>
    /// 组织架构编号
    /// </summary>
    public int Mid { get; set; }

    public int Id { get; set; }
}
