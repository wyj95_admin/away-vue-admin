﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 前端资源表
/// </summary>
public partial class SysViewResource
{
    /// <summary>
    /// 菜单编号
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 上级
    /// </summary>
    public int ParentId { get; set; }

    /// <summary>
    /// 路由名称
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// 路由地址
    /// </summary>
    public string Path { get; set; } = null!;

    /// <summary>
    /// 组件路径
    /// </summary>
    public string Component { get; set; } = null!;

    /// <summary>
    /// 路由别名
    /// </summary>
    public string Alias { get; set; } = null!;

    /// <summary>
    /// 状态:0启用；1禁用
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 跳转地址
    /// </summary>
    public string Redirect { get; set; } = null!;

    public int CaseSensitive { get; set; }

    public string Meta { get; set; } = null!;
}
