﻿using System;
using System.Collections.Generic;

namespace Away.Admin.DB;

/// <summary>
/// 管理员授权前端资源表
/// </summary>
public partial class SysAdminViewResource
{
    /// <summary>
    /// 管理员编号
    /// </summary>
    public int Oid { get; set; }

    /// <summary>
    /// 前端资源编号
    /// </summary>
    public int Mid { get; set; }

    public int Id { get; set; }
}
