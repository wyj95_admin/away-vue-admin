﻿using Away.EventBusCore.Commands;
using Away.Common.Events.Commands;

namespace Away.Admin.EventHandlers;

/// <summary>
/// 实现权限验证
/// <see cref="AuthAttribute"/>
/// </summary>
public class AuthDomainEventHandler : CommandHandler<AuthCmd, ApiResult>
{
    public AuthDomainEventHandler(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    private IAdminService AdminService => ServiceProvider.GetRequiredService<IAdminService>();

    public override async Task<ApiResult> Handle(AuthCmd request)
    {
        var search = new ApiPermSearch
        {
            AdminId = request.AdminId,
            Method = request.Method,
            Url = request.Url
        };
        if (!AdminService.ExistApiPermission(search))
        {
            return ApiResult.Ok(403, "没有该权限");
        }
        await Task.CompletedTask;
        return ApiResult.Ok();
    }
}
