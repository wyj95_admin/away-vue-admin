﻿using Away.EventBusCore.DomainEvents;

namespace Away.Common.Events.DomainEvents;

[De]
public class DemoDe : DomainEvent
{
    public override string EventId => nameof(DemoDe);
    public required string Data { get; set; }
}
