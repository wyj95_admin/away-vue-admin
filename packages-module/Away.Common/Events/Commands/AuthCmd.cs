﻿namespace Away.Common.Events.Commands;

[Cmd]
public class AuthCmd : Command<ApiResult>
{
    public override string EventId => nameof(AuthCmd);
    public int AdminId { get; set; }
    public required string Method { get; set; }
    public required string Url { get; set; }
}