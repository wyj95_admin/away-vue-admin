﻿namespace Away.Common.Events.Commands;

[Cmd]
public class DemoCmd : Command
{
    public override string EventId => nameof(DemoCmd);
    public required string Data { get; set; }
}