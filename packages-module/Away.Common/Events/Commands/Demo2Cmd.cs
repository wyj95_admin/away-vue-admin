﻿namespace Away.Common.Events.Commands;

public class EventResult
{
    public int Id { get; set; }
    public required string Message { get; set; }
}

[Cmd]
public class Demo2Cmd : Command<EventResult>
{
    public required string DDD { get; set; }

    public override string EventId => nameof(Demo2Cmd);
}