﻿using System.Security.Cryptography;

namespace Away.Common.Utils;

public sealed class Md5Builder
{
    public static string Build(string text)
    {
        var md5 = MD5.Create();
        md5.ComputeHash(Encoding.Default.GetBytes(text));
        byte[] b = md5.Hash!;
        md5.Clear();
        StringBuilder sb = new(32);
        for (int i = 0; i < b.Length; i++)
        {
            sb.Append(b[i].ToString("X2"));
        }
        return sb.ToString();
    }
}
