﻿namespace Away.Common.Mvc.Controllers;

public abstract class ApiController : ControllerBase
{
    /// <summary>
    /// 返回接口数据
    /// </summary>
    /// <typeparam name="T">数据模型</typeparam>
    /// <param name="code">状态码 0成功</param>
    /// <param name="message">说明</param>
    /// <param name="data">数据</param>
    /// <returns></returns>
    protected static ActionResult ApiOk<T>(T data, int code = 0, string message = "")
    {
        return ApiResult.Ok(data, code, message);
    }

    /// <summary>
    /// 返回接口数据
    /// </summary>   
    /// <param name="code">状态码 0成功</param>
    /// <param name="message">说明</param>
    /// <returns></returns>
    protected static ActionResult ApiOk(int code = 0, string message = "")
    {
        return ApiResult.Ok(code, message);
    }

    protected static ActionResult ApiOk(bool success)
    {
        return success ? ApiOk() : ApiOk(1, "");
    }

    protected int GetUserId()
    {
        var userId = User.Claims.FirstOrDefault(o => o.Type == Auth.JwtClaimNames.Uid)?.Value;
        return Convert.ToInt32(userId);
    }
}