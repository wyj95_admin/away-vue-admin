﻿namespace Away.Common.Mvc.Auth;

/// <summary>
/// 允许跳过权限过滤
/// </summary>
public class AllowAttribute : AllowAnonymousAttribute { }

