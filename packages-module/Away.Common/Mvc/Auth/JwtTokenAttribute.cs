﻿using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Away.Common.Mvc.Auth;

/// <summary>
/// 令牌验证
/// </summary>
public class JwtTokenAttribute : AuthorizeAttribute
{
    public JwtTokenAttribute()
    {
        AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme;
    }
}