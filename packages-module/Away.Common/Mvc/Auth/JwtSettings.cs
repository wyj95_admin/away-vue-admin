﻿using Microsoft.IdentityModel.Tokens;

namespace Away.Common.Mvc.Auth;
public class JwtSettings
{
    private string _secretKey = string.Empty;
    public required string Audience { get; set; }
    public required string Issuer { get; set; }

    public string SecretKey
    {
        get => string.IsNullOrWhiteSpace(_secretKey) ? string.Empty : _secretKey.Trim();
        set => _secretKey = value;
    }

    /// <summary>
    /// 有效期/小时
    /// </summary>
    public int Expires { get; set; }

    [JsonIgnore]
    public SymmetricSecurityKey SigningKey => new(Encoding.UTF8.GetBytes(SecretKey));
}