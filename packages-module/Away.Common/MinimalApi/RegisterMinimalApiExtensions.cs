﻿using Away.Common.MinimalApi;

namespace Microsoft.AspNetCore.Routing;

public static class RegisterMinimalApiExtensions
{
    public static IEndpointRouteBuilder MapMinimalApi(this IEndpointRouteBuilder endpoint)
    {
        var assemblies = AppDomain.CurrentDomain.GetAssemblies();

        var types = assemblies.SelectMany(o => o.DefinedTypes.Where(oo => oo.GetInterfaces().Any(ooo => ooo.Name == nameof(IRegisterMinimalApi))));

        foreach (var type in types)
        {
            var registerMinimalApi = Activator.CreateInstance(type.AsType()) as IRegisterMinimalApi;
            registerMinimalApi?.RegisterApi(endpoint);
        }
        return endpoint;
    }
}
