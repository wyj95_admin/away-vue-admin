﻿using Microsoft.AspNetCore.Http;

namespace Away.Common.MinimalApi;

public class ApiResults
{
    public static IResult Data(object value)
    {
        return TypedResults.Json(value);
    }

    public static IResult Page<T>(int total, List<T> items)
    {
        return Ok(new PageResultModel<T>()
        {
            Total = total,
            Items = items
        });
    }

    public static IResult Ok(int code = 0, string message = "")
    {
        var result = new ApiResultModel { Code = code, Message = message };
        return Data(result);
    }

    public static IResult Ok<T>(T data, int code = 0, string message = "")
    {
        var result = new ApiResultModel<T> { Code = code, Message = message, Result = data };
        return Data(result);
    }
}

