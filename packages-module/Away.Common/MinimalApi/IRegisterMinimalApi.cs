﻿using Microsoft.AspNetCore.Routing;

namespace Away.Common.MinimalApi;

/// <summary>
/// 注册最小webapi
/// 注册过程详见：<see cref="RegisterMinimalApiExtensions"/>
/// </summary>
public interface IRegisterMinimalApi
{
    void RegisterApi(IEndpointRouteBuilder endpoint);
}
