﻿namespace Away.Common.Repository;

/// <summary>
/// 基础仓储
/// </summary>
public interface IRepository
{
    void Add<TEntity>(params TEntity[] entitys) where TEntity : class;
    void Remove<TEntity>(params TEntity[] entitys) where TEntity : class;
    void Update<TEntity>(params TEntity[] entitys) where TEntity : class;
}
