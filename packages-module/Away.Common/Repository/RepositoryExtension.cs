﻿namespace Away.Common.Repository;

public abstract class BasePage
{
    private int _page = 0;
    private int _pageSize = 10;
    public int Page
    {
        get => _page <= 0 ? 1 : _page;
        set => _page = value;
    }
    public int PageSize
    {
        get => _pageSize <= 0 || _page > 100 ? 10 : _pageSize;
        set => _pageSize = value;
    }
}

public static class RepositoryExtension
{
    public static IQueryable<TSource> Page<TSource>(this IQueryable<TSource> source, BasePage page)
    {
        return source.Skip((page.Page - 1) * page.PageSize).Take(page.PageSize);
    }
}
