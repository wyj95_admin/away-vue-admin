﻿using System.Net.Mail;

namespace Away.Common.Notify.Email;

/// <summary>
/// 邮件服务
/// </summary>
public interface IEmailNotifyService
{
    /// <summary>
    /// 发送
    /// </summary>
    /// <param name="target">目标</param>
    /// <param name="action"></param>
    /// <returns></returns>
    bool Send(string target, Action<MailMessage> action);
    /// <summary>
    /// 发送
    /// </summary>
    /// <param name="target">目标</param>
    /// <param name="title">标题</param>
    /// <param name="content">内容</param>
    /// <returns></returns>
    bool Send(string target, string title, string content);
}
