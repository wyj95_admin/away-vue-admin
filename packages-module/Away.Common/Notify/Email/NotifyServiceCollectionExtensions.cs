﻿using Microsoft.Extensions.Configuration;

namespace Away.Common.Notify.Email;

public static class NotifyServiceCollectionExtensions
{
    /// <summary>
    /// 添加邮件通知服务
    /// </summary>
    /// <returns></returns>
    /// <remarks>
    /// <code>
    /// "EmailSettings": {
    ///     "Host": "smtp.qq.com",
    ///     "Account": "1591984192@qq.com",
    ///     "Password": "gajscpmvqlybjggg"
    /// }
    /// </code>
    /// </remarks>     
    public static IServiceCollection AddNotifyEmail(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<EmailSettings>(configuration.GetSection("EmailSettings"));
        services.AddSingleton<IEmailNotifyService, EmailNotifyService>();
        return services;
    }
}
