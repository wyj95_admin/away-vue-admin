﻿namespace Away.QQ.Handlers;

public class EventPrivateMessageHandler : INotificationHandler<EventPrivateMessage>
{
    private readonly ILogger<EventPrivateMessageHandler> logger;

    public EventPrivateMessageHandler(ILogger<EventPrivateMessageHandler> logger)
    {
        this.logger = logger;
    }

    public Task Handle(EventPrivateMessage notification, CancellationToken cancellationToken)
    {
        logger.LogInformation($"私聊消息-{notification.sender.nickname}：{notification.message}");
        return Task.CompletedTask;
    }
}
