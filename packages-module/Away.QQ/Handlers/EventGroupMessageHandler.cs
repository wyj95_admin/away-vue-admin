﻿namespace Away.QQ.Handlers;

public class EventGroupMessageHandler : INotificationHandler<EventGroupMessage>
{
    private readonly ILogger<EventGroupMessageHandler> logger;

    public EventGroupMessageHandler(ILogger<EventGroupMessageHandler> logger)
    {
        this.logger = logger;
    }

    public Task Handle(EventGroupMessage notification, CancellationToken cancellationToken)
    {
        logger.LogInformation($"群消息-{notification.sender.card}({notification.sender.nickname})：{notification.message}");
        return Task.CompletedTask;
    }
}
