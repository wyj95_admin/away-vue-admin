﻿using Microsoft.AspNetCore.Builder;

namespace Away.Manager;

public static class WebAppBuilderExtensions
{
    /// <summary>
    /// 添加QQ聊天模块
    /// </summary>
    /// <param name="builder"></param>
    /// <returns></returns>
    /// <remarks>
    /// <code>
    /// "QQSettings": {
    ///     "Host": "http://localhost:5700"
    /// }
    /// </code>
    /// </remarks>     
    public static WebApplicationBuilder AddAwayManagerQQModule(this WebApplicationBuilder builder)
    {
        builder.Services.Configure<QQSettings>(builder.Configuration.GetSection("QQSettings"));
        return builder;
    }
}
