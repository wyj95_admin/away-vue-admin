﻿using Microsoft.Extensions.Options;

namespace Away.QQ.Core;

/// <summary>
/// QQ发送消息
/// </summary>
[ServiceInject(ServiceLifetime.Transient)]
public class QQChat
{
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly QQSettings qqSettings;

    private string HOST => qqSettings.Host;

    public QQChat(IHttpClientFactory httpClientFactory, IOptions<QQSettings> options)
    {
        _httpClientFactory = httpClientFactory;
        qqSettings = options.Value;
    }

    /// <summary>
    /// 私聊
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<QQResult> SendPrivateMsg(PrivateMsgModel model)
    {
        using var httpclient = _httpClientFactory.CreateClient();
        var resp = await httpclient.PostAsJsonAsync($"{HOST}/send_private_msg", model);
        var res = await resp.Content.ReadFromJsonAsync<QQResult>();
        return res;
    }

    /// <summary>
    /// 群聊
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<QQResult> SendGroupMsg(GroupMsgModel model)
    {
        using var httpclient = _httpClientFactory.CreateClient();
        var resp = await httpclient.PostAsJsonAsync($"{HOST}/send_group_msg", model);
        var res = await resp.Content.ReadFromJsonAsync<QQResult>();
        return res;
    }

    /// <summary>
    /// 发送聊天消息
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<QQResult> SendMsg(MsgModel model)
    {
        using var httpclient = _httpClientFactory.CreateClient();
        var resp = await httpclient.PostAsJsonAsync($"{HOST}/send_msg", model);
        var res = await resp.Content.ReadFromJsonAsync<QQResult>();
        return res;
    }

    /// <summary>
    /// 删除聊天消息
    /// </summary>
    /// <param name="messageId"></param>
    /// <returns></returns>
    public async Task DeleteMsg(int messageId)
    {
        using var httpclient = _httpClientFactory.CreateClient();
        var resp = await httpclient.GetAsync($"{HOST}/delete_msg?{messageId}");
    }
}