﻿namespace Away.QQ.Core;

public class EventBase : INotification
{
    /// <summary>
    /// 事件发生的时间戳
    /// </summary>
    public long time { get; set; }

    /// <summary>
    /// 收到事件的机器人的 QQ 号
    /// </summary>
    public long self_id { get; set; }

    /// <summary>
    /// 表示该上报的类型, 消息, 请求, 通知, 或元事件
    /// <br/> 例如：message, request, notice, meta_event
    /// </summary>
    public string post_type { get; set; }
}

public class EventMessage : EventBase
{
    /// <summary>
    /// 消息类型
    /// <br>group</br>
    /// </summary>
    public string message_type { get; set; }

    /// <summary>
    /// 表示消息的子类型
    /// <br/>如果是好友则是 friend, 如果是群临时会话则是 group, 如果是在群中自身发送则是 group_self, 正常群聊消息是 normal, 匿名消息是 anonymous, 系统提示 ( 如「管理员已禁止群内匿名聊天」 ) 是 notice
    /// </summary>
    public string sub_type { get; set; }

    public int message_id { get; set; }

    public long user_id { get; set; }

    public string message { get; set; }

    public string raw_message { get; set; }

    public int font { get; set; }
}

public class PrivateSender
{
    /// <summary>
    /// 发送者 QQ 号
    /// </summary>
    public long user_id { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    public string nickname { get; set; }

    /// <summary>
    /// 性别, male 或 female 或 unknown
    /// </summary>
    public string sex { get; set; }

    public int age { get; set; }
}

public class GroupSender : PrivateSender
{
    /// <summary>
    /// 群名片／备注
    /// </summary>
    public string card { get; set; }

    /// <summary>
    /// 地区
    /// </summary>
    public string area { get; set; }

    /// <summary>
    /// 成员等级
    /// </summary>
    public string level { get; set; }

    /// <summary>
    ///  角色, owner 或 admin 或 member
    /// </summary>
    public string role { get; set; }

    /// <summary>
    /// 专属头衔
    /// </summary>
    public string title { get; set; }
}

public class EventPrivateMessage : EventMessage
{
    /// <summary>
    /// 临时会话来源
    /// </summary>
    public int temp_source { get; set; }
    public PrivateSender sender { get; set; }
}

public class EventGroupMessage : EventMessage
{
    /// <summary>
    /// 群号
    /// </summary>
    public int group_id { get; set; }

    /// <summary>
    /// 匿名信息, 如果不是匿名消息则为 null
    /// </summary>
    public Hashtable anonymous { get; set; }

    public GroupSender sender { get; set; }
}


public class EventRequest
{
    /// <summary>
    /// 请求类型
    /// <br/> friend好友请求  group群请求
    /// </summary>
    public string request_type { get; set; }
}

public class EventNotice
{
    /// <summary>
    /// 通知类型
    ///<br/> group_upload 群文件上传
    ///<br/> group_admin 群管理员变更
    ///<br/> group_decrease 群成员减少
    ///<br/> group_increase 群成员增加
    ///<br/> group_ban 群成员禁言
    ///<br/> friend_add 好友添加
    ///<br/> group_recall 群消息撤回
    ///<br/> friend_recall 好友消息撤回
    ///<br/> group_card 群名片变更
    ///<br/> offline_file 离线文件上传
    ///<br/> client_status 客户端状态变更
    ///<br/> essence 精华消息
    ///<br/> notify 系统通知
    /// </summary>
    public string notice_type { get; set; }
}

public class EventMeat
{
    /// <summary>
    /// 元数据类型
    /// <br/> lifecycle	生命周期
    /// <br/> heartbeat 心跳包
    /// </summary>
    public string meta_event_type { get; set; }
}