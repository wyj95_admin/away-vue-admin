﻿namespace Away.QQ.Core;

[ServiceInject(ServiceLifetime.Scoped)]
public class QQEventListener
{
    private readonly IMediator mediator;

    public QQEventListener(IMediator mediator)
    {
        this.mediator = mediator;
    }

    public Task Dispatcher(Hashtable data)
    {
        if (data == null)
        {
            return Task.CompletedTask;
        }

        var json = JsonSerializer.Serialize(data);

        if (data["post_type"].ToString() == "message")
        {

            if (data["message_type"].ToString() == "group")
            {
                var model = JsonSerializer.Deserialize<EventGroupMessage>(json);
                return mediator.Publish(model);

            }

            if (data["message_type"].ToString() == "private")
            {
                var model = JsonSerializer.Deserialize<EventPrivateMessage>(json);
                return mediator.Publish(model);
            }
        }
        //switch (eventbase.post_type)
        //{
        //    case "message":
        //        break;
        //    case "request":
        //        break;
        //    case "notice":
        //        break;
        //    case "meta_event":
        //        break;
        //}
        return Task.CompletedTask;
    }
}
