﻿namespace Away.QQ.Core;

public class QQSettings
{
    private string host;
    public string Host { get => host ?? string.Empty; set => host = value; }
}
