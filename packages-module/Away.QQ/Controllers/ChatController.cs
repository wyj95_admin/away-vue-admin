﻿namespace Away.QQ.Controllers;

/// <summary>
/// QQ聊天接口
/// </summary>
[Route("qq/chat"), Tags("QQChat")]
public class ChatController : ApiControllerBase
{
    private readonly QQChat _qqChat;
    public ChatController(QQChat qqChat)
    {
        _qqChat = qqChat;
    }

    /// <summary>
    /// 私聊
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("private/msg")]
    public async Task<ActionResult> SendPrivateMsg(PrivateMsgModel model)
    {
        var res = await _qqChat.SendPrivateMsg(model);
        return ApiOk(res.data, res.retcode, res.msg);
    }

    /// <summary>
    /// 群聊
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("group/msg")]
    public async Task<ActionResult> SendGroupMsg(GroupMsgModel model)
    {
        var res = await _qqChat.SendGroupMsg(model);
        return ApiOk(res.data, res.retcode, res.msg);
    }

    /// <summary>
    /// 发送消息
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("msg")]
    public async Task<ActionResult> SendMsg(MsgModel model)
    {
        var res = await _qqChat.SendMsg(model);
        return ApiOk(res.data, res.retcode, res.msg);
    }
}
