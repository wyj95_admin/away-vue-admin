﻿namespace Away.QQ.Controllers;

/// <summary>
/// QQ消息回调接口
/// </summary>
[Route("qq/notify"), Tags("QQChat")]
public class NotifyController : ApiControllerBase
{
    private readonly QQEventListener _qqEventListener;

    public NotifyController(QQEventListener qqEventListener)
    {
        _qqEventListener = qqEventListener;
    }

    /// <summary>
    /// 接收QQ回调数据
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    [HttpPost, Allow]
    public async Task<ActionResult> Recevie(Hashtable data)
    {
        await _qqEventListener.Dispatcher(data);
        return Ok();
    }
}
