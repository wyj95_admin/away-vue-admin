﻿using Away.Common.Mvc.Controllers;
using Away.Common.Mvc.Logger;
using Away.Common.Mvc.Validation;

namespace Away.QQ.Controllers;

[ApiController, JwtToken, Auth, ExceptionLogger, RequestRawLogger, ValidatResult]
public abstract class ApiControllerBase : ApiController
{
    protected int AdminId => GetUserId();
}