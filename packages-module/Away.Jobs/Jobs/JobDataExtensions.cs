﻿namespace Quartz;

public static class JobDataExtensions
{
    public static TModel? ToModel<TModel>(this JobDataMap jobData) where TModel : class
    {
        var json = JsonSerializer.Serialize(jobData);

        var jsonOptions = new System.Text.Json.JsonSerializerOptions
        {
            WriteIndented = true,
            PropertyNameCaseInsensitive = true,
            NumberHandling = JsonNumberHandling.AllowReadingFromString,
        };
        return JsonSerializer.Deserialize<TModel>(json, jsonOptions);
    }
}
