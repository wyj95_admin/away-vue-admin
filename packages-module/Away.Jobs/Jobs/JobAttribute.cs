﻿namespace Quartz;

[AttributeUsage(AttributeTargets.Class)]
public class JobAttribute(string name, string? group = null, string? description = null) : Attribute
{
    public string? Description { get; set; } = description;
    public string Name { get; set; } = name;
    public string? Group { get; set; } = group;

    public JobKey JobKey => JobKey.Create(Name, Group);
}
