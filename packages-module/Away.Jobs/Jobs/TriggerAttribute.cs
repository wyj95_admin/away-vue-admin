﻿using System.Web;

namespace Quartz;
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class TriggerAttribute(string name, string cron, string? data = null, string? description = null) : Attribute
{
    public string? Description { get; set; } = description;

    public string Name { get; set; } = name;
    public string Cron { get; set; } = cron;

    public string? Data { get; set; } = data;

    public JobDataMap? JobDataMap
    {
        get
        {
            if (string.IsNullOrWhiteSpace(Data))
            {
                return null;
            }

            var data = new JobDataMap();
            var query = HttpUtility.ParseQueryString(Data);
            foreach (var k in query.AllKeys)
            {
                data!.TryAdd(k, query[k]!);
            }
            return data;
        }
    }
}
