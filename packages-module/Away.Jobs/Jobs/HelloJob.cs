﻿namespace Away.Jobs;

[Job(nameof(HelloJob), "system", "测试任务")]
//[Trigger("0/10 * * * * ? ", "a=jack&b=1&c=false", "测试")]
[PersistJobDataAfterExecution, DisallowConcurrentExecution]
public class HelloJob : IJob
{
    private readonly ILogger<HelloJob> _logger;
    public HelloJob(ILogger<HelloJob> logger)
    {
        _logger = logger;
    }

    public Task Execute(IJobExecutionContext context)
    {
        _logger.LogInformation("I'm {}", context.JobDetail.Description);
        var data = JsonSerializer.Serialize(context.MergedJobDataMap);
        _logger.LogInformation("{}", data);
        return Task.CompletedTask;
    }
}
