﻿namespace Away.Jobs.Listener;

public class JobListener : JobListenerSupport
{
    public override string Name => nameof(JobListener);
}
