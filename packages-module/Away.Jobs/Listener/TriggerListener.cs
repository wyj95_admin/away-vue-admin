﻿namespace Away.Jobs.Listener;

public class TriggerListener : TriggerListenerSupport
{
    public override string Name => nameof(TriggerListener);
}
