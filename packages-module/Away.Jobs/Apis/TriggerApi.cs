﻿using Away.Manager.Common.MinimalApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace Away.Manager.Jobs.Apis;

public class TriggerApi : IRegisterMinimalApi
{
    public void RegisterApi(IEndpointRouteBuilder endpoint)
    {
        endpoint.MapGet("trigger", GetTriggerPage);
        endpoint.MapPost("trigger", AddTrigger);
        endpoint.MapPut("trigger", UpdateTrigger);
        endpoint.MapDelete("trigger", DeleteTrigger);

        var triggers = endpoint.MapGroup("trigger");
        triggers.MapGet("/list", GetTriggerGroupList);
        triggers.MapPost("/start", StartTriggers);
        triggers.MapPost("/stop", StopTriggers);
        triggers.MapPost("run", RunTrigger);

        var triggerGroups = triggers.MapGroup("group");
        triggerGroups.MapGet("start", StartTrigger);
        triggerGroups.MapGet("stop", StopTrigger);
    }

    /// <summary>
    /// 获取触发器分组列表
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> GetTriggerGroupList([FromServices] ITriggerService triggerService)
    {
        var data = await triggerService.GetTriggerGroupList();
        return ApiResults.Ok(data);
    }

    /// <summary>
    /// 查询触发器分页列表
    /// </summary>
    /// <returns></returns>
    public static IResult GetTriggerPage([FromServices] ITriggerService triggerService, [AsParameters] TriggerSearch search)
    {
        var data = triggerService.GetTriggerPage(search);
        return ApiResults.Data(data);
    }

    /// <summary>
    /// 添加触发器
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> AddTrigger([FromServices] ITriggerService triggerService, [FromBody] TriggerModel model)
    {
        await triggerService.AddTrigger(model);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 更新触发器
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> UpdateTrigger([FromServices] ITriggerService triggerService, [FromBody] TriggerModel model)
    {
        await triggerService.UpdateTrigger(model);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 启动分组触发器
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> StartTrigger([FromServices] ITriggerService triggerService, string group)
    {
        await triggerService.StartTrigger(group);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 启动触发器
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> StartTriggers([FromServices] ITriggerService triggerService, [FromBody] List<Key> keys)
    {
        await triggerService.StartTrigger(keys);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 停止分组触发器
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> StopTrigger([FromServices] ITriggerService triggerService, string group)
    {
        await triggerService.StopTrigger(group);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 停止触发器
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> StopTriggers([FromServices] ITriggerService triggerService, [FromBody] List<Key> keys)
    {
        await triggerService.StopTrigger(keys);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 删除触发器
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> DeleteTrigger([FromServices] ITriggerService triggerService, [FromBody] List<Key> keys)
    {
        await triggerService.DeleteTrigger(keys);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 立即触发
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> RunTrigger([FromServices] ITriggerService triggerService, [FromBody] Key key)
    {
        await triggerService.RunTrigger(key);
        return ApiResults.Ok();
    }
}
