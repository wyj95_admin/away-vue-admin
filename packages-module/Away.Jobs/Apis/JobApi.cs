﻿using Away.Manager.Common.MinimalApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace Away.Manager.Jobs.Apis;

public class JobApi : IRegisterMinimalApi
{
    public void RegisterApi(IEndpointRouteBuilder endpoint)
    {
        endpoint.MapGet("job", GetJobPage);
        endpoint.MapPost("job", AddJob);
        endpoint.MapPut("job", UpdateJob);

        var jobs = endpoint.MapGroup("job");
        jobs.MapPost("/cancel", Cancel);
        jobs.MapPost("/start", StartJobs);
        jobs.MapPost("/stop", StopJobs);
        jobs.MapPost("/run", TriggerJob);


        jobs.MapDelete("/group", DeleteJob);
        var jobgroups = jobs.MapGroup("group");
        jobgroups.MapGet("/list", GetJobGroupList);
        jobgroups.MapGet("/start", StartJob);
        jobgroups.MapGet("/stop", StopJob);
    }

    /// <summary>
    /// 获取工作分组列表
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> GetJobGroupList([FromServices] IJobService jobService)
    {
        var data = await jobService.GetJobGroupList();
        return ApiResults.Ok(data);
    }

    /// <summary>
    /// 获取工作分页列表
    /// </summary>
    /// <returns></returns>
    public static IResult GetJobPage([FromServices] IJobService jobService, [AsParameters] JobSearch search)
    {
        var data = jobService.GetJobPage(search);
        return ApiResults.Data(data);
    }

    /// <summary>
    /// 添加工作
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> AddJob([FromServices] IJobService jobService, [FromBody] JobDetail jobDetail)
    {
        await jobService.AddJob(jobDetail);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 更新工作
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> UpdateJob([FromServices] IJobService jobService, [FromBody] JobDetail jobDetail)
    {
        await jobService.UpdateJob(jobDetail);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 启用分组工作
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> StartJob([FromServices] IJobService jobService, string group)
    {
        await jobService.StartJob(group);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 停用分组工作
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> StopJob([FromServices] IJobService jobService, string group)
    {
        await jobService.StopJob(group);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 启用
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> StartJobs([FromServices] IJobService jobService, [FromBody] List<Key> jobKeys)
    {
        await jobService.StartJob(jobKeys);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 停用
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> StopJobs([FromServices] IJobService jobService, [FromBody] List<Key> jobKeys)
    {
        await jobService.StopJob(jobKeys);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 删除工作
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> DeleteJob([FromServices] IJobService jobService, [FromBody] List<Key> jobKeys)
    {
        await jobService.DeleteJob(jobKeys);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 立即触发工作
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> TriggerJob([FromServices] IJobService jobService, [FromBody] Key jobKey)
    {
        await jobService.RunJob(jobKey);
        return ApiResults.Ok();
    }

    /// <summary>
    /// 取消工作
    /// </summary>
    /// <returns></returns>
    public static async Task<IResult> Cancel([FromServices] IJobService jobService, [FromBody] Key jobKey)
    {
        await jobService.CancelJob(jobKey);
        return ApiResults.Ok();
    }
}
