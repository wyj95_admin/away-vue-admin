﻿namespace Away.Jobs.Repositories.Models;

public class JobDetail
{
    public string SchedName { get; set; } = null!;
    public string JobName { get; set; } = null!;
    public string JobGroup { get; set; } = null!;
    public string? Description { get; set; }
    public string JobClassName { get; set; } = null!;
    public bool IsDurable { get; set; }
    public bool IsNonconcurrent { get; set; }
    public bool IsUpdateData { get; set; }
    public bool RequestsRecovery { get; set; }
    public string? JobData { get; set; }

    [JsonIgnore]
    public JobKey Key => JobKey.Create(JobName, JobGroup);
    [JsonIgnore]
    public IJobDetail Job
    {
        get
        {
            var job = JobBuilder.Create()
                .WithIdentity(Key)
                .WithDescription(Description)
                .StoreDurably(IsDurable)
                .RequestRecovery(RequestsRecovery)
                .OfType(Type.GetType(JobClassName)!);


            if (!string.IsNullOrWhiteSpace(JobData))
            {
                var jobData = JsonSerializer.Deserialize<JobDataMap>(JobData);
                job.UsingJobData(jobData!);
            }

            return job.Build();
        }
    }
}
