﻿namespace Away.Jobs.Repositories.Models;

public class CronTriggerModel : BaseTrigger
{
    private string _cron = string.Empty;
    public string CronExpression
    {
        get => string.IsNullOrWhiteSpace(_cron) ? string.Empty : _cron.Trim();
        set => _cron = value;
    }
    public string? TimeZoneId { get; set; }
}
