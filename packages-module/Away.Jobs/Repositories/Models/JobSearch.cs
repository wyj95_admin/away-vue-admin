﻿namespace Away.Jobs.Repositories.Models;

public class JobSearch : BasePage
{
    private string _jobname = string.Empty;
    private string _jobgroup = string.Empty;
    private string _jobClassName = string.Empty;
    public string JobName
    {
        get => string.IsNullOrWhiteSpace(_jobname) ? string.Empty : _jobname.Trim();
        set => _jobname = value;
    }

    public string JobGroup
    {
        get => string.IsNullOrWhiteSpace(_jobgroup) ? string.Empty : _jobgroup.Trim();
        set => _jobgroup = value;
    }

    public string JobClassName
    {
        get => string.IsNullOrWhiteSpace(_jobClassName) ? string.Empty : _jobClassName.Trim();
        set => _jobClassName = value;
    }
}
