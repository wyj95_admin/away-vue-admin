﻿namespace Away.Jobs.Repositories.Models;

public class SimpleTriggerModel : BaseTrigger
{
    public long RepeatCount { get; set; }
    public long RepeatInterval { get; set; }
    public long TimesTriggered { get; set; }
}
