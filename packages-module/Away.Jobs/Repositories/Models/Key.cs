﻿using Quartz.Util;

namespace Away.Jobs.Repositories.Models;

public class Key : Key<Key>
{
    [JsonIgnore]
    public TriggerKey TriggerKey => new(Name, Group);
    [JsonIgnore]
    public JobKey JobKey => new(Name, Group);
}
