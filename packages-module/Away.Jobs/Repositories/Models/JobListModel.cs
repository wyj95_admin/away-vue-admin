﻿namespace Away.Jobs.Repositories.Models;

public class JobListModel
{
    public string SchedName { get; set; } = null!;
    public string JobName { get; set; } = null!;
    public string JobGroup { get; set; } = null!;
    public string? Description { get; set; }
}
