﻿namespace Away.Jobs.Repositories.Models;

public class BlobTriggerModel : BaseTrigger
{
    public string? BlobData { get; set; }
}
