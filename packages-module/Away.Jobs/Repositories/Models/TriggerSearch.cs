﻿namespace Away.Jobs.Repositories.Models;

public class TriggerSearch : BasePage
{
    private string _triggerName = string.Empty;
    private string _triggerGroup = string.Empty;
    private string _jobname = string.Empty;
    private string _jobgroup = string.Empty;

    public string JobName
    {
        get => string.IsNullOrWhiteSpace(_jobname) ? string.Empty : _jobname.Trim();
        set => _jobname = value;
    }
    public string JobGroup
    {
        get => string.IsNullOrWhiteSpace(_jobgroup) ? string.Empty : _jobgroup.Trim();
        set => _jobgroup = value;
    }
    public string TriggerName
    {
        get => string.IsNullOrWhiteSpace(_triggerName) ? string.Empty : _triggerName.Trim();
        set => _triggerName = value;
    }
    public string TriggerGroup
    {

        get => string.IsNullOrWhiteSpace(_triggerGroup) ? string.Empty : _triggerGroup.Trim();
        set => _triggerGroup = value;
    }
}
