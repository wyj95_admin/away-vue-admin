﻿namespace Away.Jobs.Repositories.Models;

public class SimpropTriggerModel : BaseTrigger
{
    public string? StrProp1 { get; set; }
    public string? StrProp2 { get; set; }
    public string? StrProp3 { get; set; }
    public int? IntProp1 { get; set; }
    public int? IntProp2 { get; set; }
    public long? LongProp1 { get; set; }
    public long? LongProp2 { get; set; }
    public decimal? DecProp1 { get; set; }
    public decimal? DecProp2 { get; set; }
    public bool? BoolProp1 { get; set; }
    public bool? BoolProp2 { get; set; }
    public string? TimeZoneId { get; set; }
}
