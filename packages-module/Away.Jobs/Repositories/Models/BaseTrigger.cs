﻿namespace Away.Jobs.Repositories.Models;

public abstract class BaseTrigger
{
    private string _schedName = string.Empty;
    private string _triggerName = string.Empty;
    private string _triggerGroup = string.Empty;

    [JsonIgnore]
    public string SchedName
    {
        get => string.IsNullOrWhiteSpace(_schedName) ? string.Empty : _schedName.Trim();
        set => _schedName = value;
    }
    [JsonIgnore]
    public string TriggerName
    {
        get => string.IsNullOrWhiteSpace(_triggerName) ? string.Empty : _triggerName.Trim();
        set => _triggerName = value;
    }
    [JsonIgnore]
    public string TriggerGroup
    {
        get => string.IsNullOrWhiteSpace(_triggerGroup) ? string.Empty : _triggerGroup.Trim();
        set => _triggerGroup = value;
    }
}
