﻿namespace Away.Jobs.Repositories;

public class JobRepositoryBase : RepositoryBase<JobsDbContext>
{
    public JobRepositoryBase(JobsDbContext context) : base(context)
    {
    }
}
