﻿namespace Away.Jobs.Repositories;

/// <summary>
/// 触发器仓储
/// </summary>
public interface ITriggerRepository
{
    /// <summary>
    /// 查询触发器分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetTriggerPage(TriggerSearch search);
}
