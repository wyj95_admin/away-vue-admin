﻿namespace Away.Jobs.Repositories;

/// <summary>
/// 工作仓储接口
/// </summary>
public interface IJobRepository
{
    /// <summary>
    /// 获取工作分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetJobPage(JobSearch search);
}
