﻿namespace Away.Jobs.Repositories.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class JobRepository : JobRepositoryBase, IJobRepository
{
    private readonly IMapper _mapper;
    public JobRepository(JobsDbContext context, IMapper mapper) : base(context)
    {
        _mapper = mapper;
    }

    public ApiResult GetJobPage(JobSearch search)
    {
        var cond = CondBuilder.New<QrtzJobDetail>(true)
            .And(!string.IsNullOrWhiteSpace(search.JobClassName), o => o.JobClassName.Contains(search.JobClassName))
            .And(!string.IsNullOrWhiteSpace(search.JobGroup), o => o.JobGroup == search.JobGroup)
            .And(!string.IsNullOrWhiteSpace(search.JobName), o => o.JobName.Contains(search.JobName));

        var tb = Query<QrtzJobDetail>().Where(cond);
        var total = tb.Count();
        var items = tb.OrderByDescending(o => o.JobName)
            .Page(search)
            .ToList()
            .Select(_mapper.Map<JobDetail>)
            .ToList();
        return ApiResult.Page(total, items);
    }
}