﻿namespace Away.Jobs.Services;

public interface IJobService
{
    /// <summary>
    /// 获取工作分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetJobPage(JobSearch search);
    /// <summary>
    /// 获取工作分组列表
    /// </summary>
    /// <returns></returns>
    Task<List<string>> GetJobGroupList();
    /// <summary>
    /// 添加工作
    /// </summary>
    /// <param name="jobDetail"></param>
    /// <returns></returns>
    Task AddJob(JobDetail jobDetail);
    /// <summary>
    /// 更新工作
    /// </summary>
    /// <param name="jobDetail"></param>
    /// <returns></returns>
    Task UpdateJob(JobDetail jobDetail);
    /// <summary>
    /// 启用分组工作
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    Task StartJob(string group);
    /// <summary>
    /// 停用分组工作
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    Task StopJob(string group);
    /// <summary>
    /// 启用
    /// </summary>
    /// <returns></returns>
    Task StartJob(List<Key> jobKeys);
    /// <summary>
    /// 停用
    /// </summary>
    /// <returns></returns>
    Task StopJob(List<Key> jobKeys);
    /// <summary>
    /// 删除工作
    /// </summary>
    /// <param name="jobKeys"></param>
    /// <returns></returns>
    Task DeleteJob(List<Key> jobKeys);
    /// <summary>
    /// 执行任务
    /// </summary>
    /// <param name="jobKey"></param>
    /// <returns></returns>
    Task RunJob(Key jobKey);
    /// <summary>
    /// 取消任务
    /// </summary>
    /// <param name="jobKey"></param>
    /// <returns></returns>
    Task CancelJob(Key jobKey);
}
