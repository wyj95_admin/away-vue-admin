﻿namespace Away.Jobs.Services;

/// <summary>
/// 触发器服务
/// </summary>
public interface ITriggerService
{
    /// <summary>
    /// 查询触发器分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetTriggerPage(TriggerSearch search);
    /// <summary>
    /// 获取触发器分组列表
    /// </summary>
    /// <returns></returns>
    Task<List<string>> GetTriggerGroupList();
    /// <summary>
    /// 添加触发器
    /// </summary>
    /// <returns></returns>
    Task AddTrigger(TriggerModel model);
    /// <summary>
    /// 更新触发器
    /// </summary>
    /// <returns></returns>
    Task UpdateTrigger(TriggerModel model);
    /// <summary>
    /// 启动分组触发器
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    Task StartTrigger(string group);
    /// <summary>
    /// 启动触发器
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    Task StartTrigger(List<Key> keys);
    /// <summary>
    /// 停止分组触发器
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    Task StopTrigger(string group);
    /// <summary>
    /// 停止触发器
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    Task StopTrigger(List<Key> keys);
    /// <summary>
    /// 删除触发器
    /// </summary>
    /// <returns></returns>
    Task DeleteTrigger(List<Key> keys);
    /// <summary>
    /// 添加触发器
    /// </summary>
    /// <param name="trigger"></param>
    /// <returns></returns>
    Task AddTrigger(ITrigger trigger);
    /// <summary>
    /// 立即触发
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    Task RunTrigger(Key key);
}
