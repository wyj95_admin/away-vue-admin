﻿global using AutoMapper;
global using Away.Common.DI;
global using Away.Common.Mvc.Controllers;
global using Away.Common.Repository;
global using Away.Common.Utils;
global using Away.Jobs.DB;
global using Away.Jobs.Repositories;
global using Away.Jobs.Repositories.Models;
global using Away.Jobs.Services;
global using Microsoft.AspNetCore.Http;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.Extensions.Logging;
global using Quartz;
global using Quartz.Impl.Matchers;
global using Quartz.Listener;
global using System.Text.Json;
global using System.Text.Json.Serialization;
