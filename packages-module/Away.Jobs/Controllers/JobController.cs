﻿namespace Away.Jobs.Controllers;

/// <summary>
/// 工作接口
/// </summary>
[Route("job"), Tags("Job")]
public class JobController : ApiControllerBase
{
    private readonly IJobService _jobService;
    public JobController(IJobService jobService)
    {
        _jobService = jobService;
    }

    /// <summary>
    /// 获取工作分组列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("group/list")]
    public async Task<IActionResult> GetJobGroupList()
    {
        var data = await _jobService.GetJobGroupList();
        return ApiOk(data);
    }
    /// <summary>
    /// 获取工作分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    [HttpGet]
    public IActionResult GetJobPage([FromQuery] JobSearch search)
    {
        return _jobService.GetJobPage(search);
    }
    /// <summary>
    /// 添加工作
    /// </summary>
    /// <param name="jobDetail"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> AddJob(JobDetail jobDetail)
    {
        await _jobService.AddJob(jobDetail);
        return ApiOk();
    }
    /// <summary>
    /// 更新工作
    /// </summary>
    /// <param name="jobDetail"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> UpdateJob(JobDetail jobDetail)
    {
        await _jobService.UpdateJob(jobDetail);
        return ApiOk();
    }
    /// <summary>
    /// 启用分组工作
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    [HttpGet("group/start")]
    public async Task<IActionResult> StartJob(string group)
    {
        await _jobService.StartJob(group);
        return ApiOk();
    }
    /// <summary>
    /// 停用分组工作
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    [HttpGet("group/stop")]
    public async Task<IActionResult> StopJob(string group)
    {
        await _jobService.StopJob(group);
        return ApiOk();
    }
    /// <summary>
    /// 启用
    /// </summary>
    /// <returns></returns>
    [HttpPost("start")]
    public async Task<IActionResult> StartJob(List<Key> jobKeys)
    {
        await _jobService.StartJob(jobKeys);
        return ApiOk();
    }
    /// <summary>
    /// 停用
    /// </summary>
    /// <returns></returns>
    [HttpPost("stop")]
    public async Task<IActionResult> StopJob(List<Key> jobKeys)
    {
        await _jobService.StopJob(jobKeys);
        return ApiOk();
    }
    /// <summary>
    /// 删除工作
    /// </summary>
    /// <param name="jobKeys"></param>
    /// <returns></returns>
    [HttpDelete]
    public async Task<IActionResult> DeleteJob(List<Key> jobKeys)
    {
        await _jobService.DeleteJob(jobKeys);
        return ApiOk();
    }
    /// <summary>
    /// 立即触发工作
    /// </summary>
    /// <param name="jobKey"></param>
    /// <returns></returns>
    [HttpPost("run")]
    public async Task<IActionResult> TriggerJob(Key jobKey)
    {
        await _jobService.RunJob(jobKey);
        return ApiOk();
    }
    /// <summary>
    /// 取消工作
    /// </summary>
    /// <param name="jobKey"></param>
    /// <returns></returns>
    [HttpPost("cancel")]
    public async Task<IActionResult> Cancel(Key jobKey)
    {
        await _jobService.CancelJob(jobKey);
        return ApiOk();
    }
}
