﻿using Away.Common.Mvc.Logger;
using Away.Common.Mvc.Validation;

namespace Away.Jobs.Controllers;

[ApiController,  ExceptionLogger, RequestRawLogger, ValidatResult]
public abstract class ApiControllerBase : ApiController
{
    protected int AdminId => GetUserId();
}