﻿namespace Away.Jobs.Controllers;

/// <summary>
/// 触发器接口
/// </summary>
[Route("trigger"), Tags("Job")]
public class TriggerController : ApiControllerBase
{
    private readonly ITriggerService _triggerService;
    public TriggerController(ITriggerService triggerService)
    {
        _triggerService = triggerService;
    }

    /// <summary>
    /// 获取触发器分组列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("list")]
    public async Task<IActionResult> GetTriggerGroupList()
    {
        var data = await _triggerService.GetTriggerGroupList();
        return ApiOk(data);
    }
    /// <summary>
    /// 查询触发器分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    [HttpGet]
    public IActionResult GetTriggerPage([FromQuery] TriggerSearch search)
    {
        return _triggerService.GetTriggerPage(search);
    }
    /// <summary>
    /// 添加触发器
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> AddTrigger(TriggerModel model)
    {
        await _triggerService.AddTrigger(model);
        return ApiOk();
    }
    /// <summary>
    /// 更新触发器
    /// </summary>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> UpdateTrigger(TriggerModel model)
    {
        await _triggerService.UpdateTrigger(model);
        return ApiOk();
    }
    /// <summary>
    /// 启动分组触发器
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    [HttpGet("group/start")]
    public async Task<IActionResult> StartTrigger(string group)
    {
        await _triggerService.StartTrigger(group);
        return ApiOk();
    }
    /// <summary>
    /// 启动触发器
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    [HttpPost("start")]
    public async Task<IActionResult> StartTrigger(List<Key> keys)
    {
        await _triggerService.StartTrigger(keys);
        return ApiOk();
    }
    /// <summary>
    /// 停止分组触发器
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    [HttpGet("group/stop")]
    public async Task<IActionResult> StopTrigger(string group)
    {
        await _triggerService.StopTrigger(group);
        return ApiOk();
    }
    /// <summary>
    /// 停止触发器
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    [HttpPost("stop")]
    public async Task<IActionResult> StopTrigger(List<Key> keys)
    {
        await _triggerService.StopTrigger(keys);
        return ApiOk();
    }
    /// <summary>
    /// 删除触发器
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    [HttpDelete]
    public async Task<IActionResult> DeleteTrigger(List<Key> keys)
    {
        await _triggerService.DeleteTrigger(keys);
        return ApiOk();
    }

    /// <summary>
    /// 立即触发
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    [HttpPost("run")]
    public async Task<IActionResult> RunTrigger(Key key)
    {
        await _triggerService.RunTrigger(key);
        return ApiOk();
    }
}
