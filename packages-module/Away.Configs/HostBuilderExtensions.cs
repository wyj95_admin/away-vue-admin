﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace Away.Configs;

public static class HostBuilderExtensions
{
    /// <summary>
    /// 添加系统配置模块
    /// </summary>
    /// <param name="builder"></param>
    public static void AddConfigs(this IHostBuilder builder)
    {
        builder.ConfigureServices((host, services) =>
        {
            var configuration = host.Configuration;
            var conn = configuration.GetConnectionString("MySql");
            var ver = ServerVersion.Parse("8.0.26");
            services.AddDbContext<ConfigsDbContext>(o => o.UseMySql(conn, ver));
            services.AddHostedService<InitTablesHostService<ConfigsDbContext>>();
            services.AddAwayDI("Away.Configs");

            builder.ConfigureHostConfiguration(o =>
            {
                o.AddAwayConfigs(o =>
                {
                    o.Environment = host.HostingEnvironment.EnvironmentName;
                    o.UseMySql(conn, ver);
                    o.UseApplicationServiceProvider(services.BuildServiceProvider());
                });
            });
        });
    }
}
