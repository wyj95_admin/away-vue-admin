﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Away.Configs.DB;

public partial class ConfigsDbContext : DbContext
{
    public ConfigsDbContext(DbContextOptions<ConfigsDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<ConfConfiguration> ConfConfigurations { get; set; }

    public virtual DbSet<ConfGroup> ConfGroups { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb4_0900_ai_ci")
            .HasCharSet("utf8mb4");

        modelBuilder.Entity<ConfConfiguration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("conf_configuration", tb => tb.HasComment("配置表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id)
                .HasComment("配置编号")
                .HasColumnName("id");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.Env)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("环境")
                .HasColumnName("env");
            entity.Property(e => e.GroupId)
                .HasComment("分组编号")
                .HasColumnName("group_id");
            entity.Property(e => e.Key)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("键名称")
                .HasColumnName("key");
            entity.Property(e => e.Remark)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("备注")
                .HasColumnName("remark");
            entity.Property(e => e.Status)
                .HasComment("状态：0启用；1禁用")
                .HasColumnName("status");
            entity.Property(e => e.UpdateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("update_time");
            entity.Property(e => e.Value)
                .HasMaxLength(900)
                .HasDefaultValueSql("''")
                .HasComment("内容")
                .HasColumnName("value");
            entity.Property(e => e.ValueType)
                .HasComment("内容类型：0值类型；1JSON；2YAML")
                .HasColumnName("value_type");
        });

        modelBuilder.Entity<ConfGroup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity
                .ToTable("conf_group", tb => tb.HasComment("分组表"))
                .UseCollation("utf8mb4_general_ci");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreateTime)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("创建时间")
                .HasColumnType("datetime")
                .HasColumnName("create_time");
            entity.Property(e => e.Env)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("环境")
                .HasColumnName("env");
            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("名称")
                .HasColumnName("name");
            entity.Property(e => e.Remark)
                .HasMaxLength(255)
                .HasDefaultValueSql("''")
                .HasComment("备注")
                .HasColumnName("remark");
            entity.Property(e => e.Status)
                .HasComment("状态：0启用；1禁用")
                .HasColumnName("status");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
