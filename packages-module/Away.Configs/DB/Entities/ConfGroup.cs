﻿using System;
using System.Collections.Generic;

namespace Away.Configs.DB;

/// <summary>
/// 分组表
/// </summary>
public partial class ConfGroup
{
    public int Id { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; } = null!;

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 环境
    /// </summary>
    public string Env { get; set; } = null!;

    /// <summary>
    /// 状态：0启用；1禁用
    /// </summary>
    public int Status { get; set; }
}
