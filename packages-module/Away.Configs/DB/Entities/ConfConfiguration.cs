﻿using System;
using System.Collections.Generic;

namespace Away.Configs.DB;

/// <summary>
/// 配置表
/// </summary>
public partial class ConfConfiguration
{
    /// <summary>
    /// 配置编号
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; } = null!;

    /// <summary>
    /// 键名称
    /// </summary>
    public string Key { get; set; } = null!;

    /// <summary>
    /// 内容
    /// </summary>
    public string Value { get; set; } = null!;

    /// <summary>
    /// 分组编号
    /// </summary>
    public int GroupId { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime UpdateTime { get; set; }

    /// <summary>
    /// 状态：0启用；1禁用
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// 内容类型：0值类型；1JSON；2YAML
    /// </summary>
    public int ValueType { get; set; }

    /// <summary>
    /// 环境
    /// </summary>
    public string Env { get; set; } = null!;
}
