﻿namespace Away.Configs.Controllers;

/// <summary>
/// 系统配置接口
/// </summary>
[Route("sys/conf")]
public class ConfigController : ApiControllerBase
{
    private readonly IConfigService _configService;
    public ConfigController(IConfigService configService)
    {
        _configService = configService;
    }

    /// <summary>
    /// 添加分组
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("group")]
    public IActionResult AddGroup(ConfGroup model)
    {
        var data = _configService.AddGroup(model);
        return ApiOk(data);
    }
    /// <summary>
    /// 修改分组
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut("group")]
    public IActionResult UpdateGroup(ConfGroup model)
    {
        var data = _configService.UpdateGroup(model);
        return ApiOk(data);
    }
    /// <summary>
    /// 删除分组
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete("group")]
    public IActionResult DeleteGroup([Required] int[] ids)
    {
        var data = _configService.DeleteGroup(ids);
        return ApiOk(data);
    }

    /// <summary>
    /// 查询分组分页列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("group")]
    public IActionResult GetGroupPage([FromQuery] GroupSearch search)
    {
        return _configService.GetGroupPage(search);
    }

    /// <summary>
    /// 查询分组列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("group/list")]
    public IActionResult GetGroupList()
    {
        var data = _configService.GetGroupList();
        return ApiOk(data);
    }

    /// <summary>
    /// 查询环境列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("env/list")]
    public IActionResult GetEnvList()
    {
        var data = _configService.GetEnvList();
        return ApiOk(data);
    }

    /// <summary>
    /// 添加配置
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("config")]
    public async ValueTask<IActionResult> AddConfig(ConfConfiguration model)
    {
        var data = await _configService.AddConfigAsync(model);
        return ApiOk(data);
    }
    /// <summary>
    /// 修改配置
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut("config"), Allow]
    public async ValueTask<IActionResult> UpdateConfig(ConfConfiguration model)
    {
        var data = await _configService.UpdateConfig(model);
        return ApiOk(data);
    }
    /// <summary>
    /// 删除配置
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete("config")]
    public async ValueTask<IActionResult> DeleteConfig([Required] int[] ids)
    {
        var data = await _configService.DeleteConfig(ids);
        return ApiOk(data);
    }
    /// <summary>
    /// 获取配置列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    [HttpGet("config")]
    public IActionResult GetConfigPage([FromQuery] ConfigSearch search)
    {
        return _configService.GetConfigPage(search);
    }

}
