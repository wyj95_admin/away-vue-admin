﻿using System.Threading.Channels;

namespace Away.Configs;

public struct ConfigNotifyMessage
{
}

/// <summary>
/// 配置通知
/// </summary>
public class ConfigNotifyChannel
{
    private static readonly Channel<ConfigNotifyMessage> _channel = Channel.CreateBounded<ConfigNotifyMessage>(10);

    public static ValueTask Publish()
    {
        return _channel.Writer.WriteAsync(new ConfigNotifyMessage());
    }

    public static void Consumer(Action<ConfigNotifyMessage> action)
    {
        Task.Run(async () =>
        {
            await foreach (var item in _channel.Reader.ReadAllAsync())
            {
                action(item);
            }
        });
    }
}
