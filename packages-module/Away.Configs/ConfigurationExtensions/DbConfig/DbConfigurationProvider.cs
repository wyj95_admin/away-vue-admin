﻿namespace Away.Configs;

public class DbConfigurationProvider : ConfigurationProvider, IDisposable
{
    private DbConfigurationSource Source { get; set; }

    public DbConfigurationProvider(DbConfigurationSource source)
    {
        Source = source ?? throw new ArgumentNullException(nameof(source));
        ConfigNotifyChannel.Consumer(notify => Load());
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    public override void Load()
    {
        Data = new DbConfigurationService(Source).Data;
        OnReload();
    }
}