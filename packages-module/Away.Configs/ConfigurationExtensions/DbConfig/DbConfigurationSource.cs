﻿using Microsoft.EntityFrameworkCore;

namespace Away.Configs;

public class DbConfigurationSource : DbContextOptionsBuilder<ConfigsDbContext>, IConfigurationSource
{
    /// <summary>
    /// 环境
    /// </summary>
    public string Environment { get; set; } = "Development";

    public IConfigurationProvider Build(IConfigurationBuilder builder)
    {
        //builder.AddInMemoryCollection(new DbConfigurationService(this).Data);
        return new DbConfigurationProvider(this);
    }
}