﻿namespace Microsoft.Extensions.Configuration;

public static class ConfigurationBuilderExtensions
{
    public static IConfigurationBuilder AddAwayConfigs(this IConfigurationBuilder builder, Action<DbConfigurationSource> action)
    {
        DbConfigurationSource _configureSource = new();
        action(_configureSource);
        return builder.Add(_configureSource);
    }
    public static IConfigurationBuilder AddAwayConfigs(this IConfigurationBuilder builder, DbConfigurationSource configureSource)
    {
        return builder.Add(configureSource);
    }
}