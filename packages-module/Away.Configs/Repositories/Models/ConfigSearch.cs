﻿namespace Away.Configs.Repositories.Models;

public class ConfigSearch : BasePage
{
    private string _key = string.Empty;
    private string _env = string.Empty;

    /// <summary>
    /// 分组编号
    /// </summary>
    public int GroupId { get; set; }
    /// <summary>
    /// 环境
    /// </summary>
    public string Env
    {
        get => string.IsNullOrWhiteSpace(_env) ? string.Empty : _env.Trim();
        set => _env = value;
    }
    /// <summary>
    /// 键名称
    /// </summary>
    public string Key
    {
        get => string.IsNullOrWhiteSpace(_key) ? string.Empty : _key.Trim();
        set => _key = value;
    }
    /// <summary>
    /// 状态：0启用；1禁用
    /// </summary>
    public int? Status { get; set; }
}
