﻿namespace Away.Configs.Repositories.Models;

public class Configuration
{
    /// <summary>
    /// 键名称
    /// </summary>
    public string Key { get; set; } = null!;

    /// <summary>
    /// 内容
    /// </summary>
    public string Value { get; set; } = null!;

    /// <summary>
    /// 内容类型：0值类型；1JSON；2YAML
    /// </summary>
    public int ValueType { get; set; }
}
