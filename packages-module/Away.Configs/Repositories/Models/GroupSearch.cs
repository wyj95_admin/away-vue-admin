﻿namespace Away.Configs.Repositories.Models;

public class GroupSearch : BasePage
{
    private string _name = string.Empty;
    private string _env = string.Empty;
    /// <summary>
    /// 名称
    /// </summary>
    public string Name
    {
        get => string.IsNullOrWhiteSpace(_name) ? string.Empty : _name.Trim();
        set => _name = value;
    }

    /// <summary>
    /// 环境
    /// </summary>
    public string Env
    {
        get => string.IsNullOrWhiteSpace(_env) ? string.Empty : _env.Trim();
        set => _env = value;
    }

    /// <summary>
    /// 状态：0启用；1禁用
    /// </summary>
    public int? Status { get; set; }
}
