﻿namespace Away.Configs.Repositories;

public class ConfRepositoryBase : RepositoryBase<ConfigsDbContext>
{
    public ConfRepositoryBase(ConfigsDbContext context) : base(context)
    {
    }
}
