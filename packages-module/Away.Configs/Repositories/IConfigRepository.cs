﻿namespace Away.Configs.Repositories;

/// <summary>
/// 系统配置仓储
/// </summary>
public interface IConfigRepository
{
    /// <summary>
    /// 添加分组
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddGroup(ConfGroup model);
    /// <summary>
    /// 修改分组
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateGroup(ConfGroup model);
    /// <summary>
    /// 删除分组
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool DeleteGroup(int[] ids);
    /// <summary>
    /// 查询环境列表
    /// </summary>
    /// <returns></returns>
    List<string> GetEnvList();
    /// <summary>
    /// 查询分组列表
    /// </summary>
    /// <returns></returns>
    List<ConfGroup> GetGroupList();
    /// <summary>
    /// 分组分页列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetGroupPage(GroupSearch search);
    /// <summary>
    /// 添加配置
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool AddConfig(ConfConfiguration model);
    /// <summary>
    /// 修改配置
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    bool UpdateConfig(ConfConfiguration model);
    /// <summary>
    /// 删除配置
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    bool DeleteConfig(int[] ids);
    /// <summary>
    /// 获取配置列表
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    ApiResult GetConfigPage(ConfigSearch search);
    /// <summary>
    /// 获取配置列表
    /// </summary>
    /// <returns></returns>
    List<Configuration> GetConfigList(string env);
}
