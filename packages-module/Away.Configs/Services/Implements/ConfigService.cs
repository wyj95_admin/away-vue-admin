﻿namespace Away.Configs.Services.Implements;

[ServiceInject(ServiceLifetime.Scoped)]
public class ConfigService : IConfigService
{
    private readonly IConfigRepository _configRepository;
    public ConfigService(IConfigRepository configRepository)
    {
        _configRepository = configRepository;
    }
    public bool UpdateGroup(ConfGroup model)
    {
        return _configRepository.UpdateGroup(model);
    }
    public bool AddGroup(ConfGroup model)
    {
        return _configRepository.AddGroup(model);
    }
    public ApiResult GetGroupPage(GroupSearch search)
    {
        return _configRepository.GetGroupPage(search);
    }
    public List<ConfGroup> GetGroupList()
    {
        return _configRepository.GetGroupList();
    }
    public List<string> GetEnvList()
    {
        return _configRepository.GetEnvList();
    }
    public bool DeleteGroup(int[] ids)
    {
        return _configRepository.DeleteGroup(ids);
    }
    public async ValueTask<bool> AddConfigAsync(ConfConfiguration model)
    {
        var res = _configRepository.AddConfig(model);
        if (res)
        {
            await ConfigNotifyChannel.Publish();
        };
        return res;
    }
    public async ValueTask<bool> DeleteConfig(int[] ids)
    {
        var res = _configRepository.DeleteConfig(ids);
        if (res)
        {
            await ConfigNotifyChannel.Publish();
        };
        return res;
    }
    public ApiResult GetConfigPage(ConfigSearch search)
    {
        return _configRepository.GetConfigPage(search);
    }
    public async ValueTask<bool> UpdateConfig(ConfConfiguration model)
    {
        var res = _configRepository.UpdateConfig(model);
        if (res)
        {
            await ConfigNotifyChannel.Publish();
        };
        return res;
    }
}
