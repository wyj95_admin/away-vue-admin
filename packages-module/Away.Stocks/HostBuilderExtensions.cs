﻿using Away.Stocks.Services.Implements;
using Microsoft.Extensions.Hosting;
using System.Text;

namespace Away.Stocks;

public static class HostBuilderExtensions
{
    /// <summary>
    /// 添加股票模块
    /// </summary>
    /// <param name="builder"></param>
    public static void AddStocks(this IHostBuilder builder)
    {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

        builder.ConfigureServices((host, services) =>
        {
            services.AddHttpClient();
            services.AddScoped<IStockService, StockService>();
        });
    }
}
