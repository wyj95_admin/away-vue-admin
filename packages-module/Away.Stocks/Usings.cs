﻿global using Away.Common.DI;
global using Away.Stocks.Services;
global using Away.Stocks.Services.Models;
global using Microsoft.AspNetCore.Builder;
global using Microsoft.AspNetCore.Http;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.AspNetCore.Routing;
global using Microsoft.Extensions.DependencyInjection;
