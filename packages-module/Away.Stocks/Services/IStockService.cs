﻿namespace Away.Stocks.Services;

/// <summary>
/// 股票服务
/// </summary>
public interface IStockService
{
    /// <summary>
    /// 获取股票信息列表
    /// </summary>
    /// <param name="stockcodes">股票代码</param>
    /// <returns></returns>
    Task<Dictionary<string, Stock>> GetStockList(string stockcodes);
}
