﻿namespace Away.Stocks.Services.Models;

/// <summary>
/// 股票行情详情
/// </summary>
public class Stock
{
    /// <summary>
    /// 股票代码
    /// </summary>
    public string? StockCode { get; set; }

    /// <summary>
    /// 股票名称
    /// </summary>
    public string? StockName { get; set; }

    /// <summary>
    /// 今日开盘价
    /// </summary>
    public decimal TodayStartPrice { get; set; }

    /// <summary>
    /// 昨日收盘价
    /// </summary>
    public decimal LastdayEndPrice { get; set; }

    /// <summary>
    /// 当前价格
    /// </summary>
    public decimal NowPrice { get; set; }

    /// <summary>
    /// 今日最高价
    /// </summary>
    public decimal TodayMaxPrice { get; set; }

    /// <summary>
    /// 今日最低价
    /// </summary>
    public decimal TodayMinPrice { get; set; }

    /// <summary>
    /// 竞买价
    /// </summary>
    public decimal BuyBiddingPrice { get; set; }

    /// <summary>
    /// 竞卖价
    /// </summary>
    public decimal SellBiddingPrice { get; set; }

    /// <summary>
    /// 成交的股票数(股）
    /// </summary>
    public int TradingNumber { get; set; }

    /// <summary>
    /// 成交金额(元)
    /// </summary>
    public decimal TradingVolume { get; set; }

    /// <summary>
    /// 五档买
    /// </summary>
    public List<MarketPriceLevel>? BuyLevel { get; set; }

    /// <summary>
    /// 五档卖
    /// </summary>
    public List<MarketPriceLevel>? SellLevel { get; set; }

    /// <summary>
    /// 时间
    /// </summary>
    public DateTime DateTime { get; set; }
}

public class MarketPriceLevel
{
    /// <summary>
    /// 价格
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// 股数
    /// </summary>
    public int Number { get; set; }

    public MarketPriceLevel()
    { }

    public MarketPriceLevel(decimal price, int number)
    {
        Price = price;
        Number = number;
    }
}