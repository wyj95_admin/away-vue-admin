﻿using Away.Common.MinimalApi;

namespace Away.Stocks.Apis;

public class StockApi : IRegisterMinimalApi
{
    public void RegisterApi(IEndpointRouteBuilder endpoint)
    {
        var group = endpoint.MapGroup("stock");
        group.MapGet("/list", GetStockList);

    }

    /// <summary>
    /// 获取股票列表
    /// </summary>
    /// <returns></returns>
    private async Task<IResult> GetStockList([FromServices] IStockService service, string stockcodes)
    {
        var data = await service.GetStockList(stockcodes);
        return Results.Json(data);
    }
}


