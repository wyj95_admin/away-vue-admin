#!/bin/bash
set -e
set -x

#dotnet tool install --global dotnet-ef
#dotnet tool update --global dotnet-ef

CONN="Host=192.168.64.201;Database=Away.Manager;Username=root;Password=123456"
Project="./manager-host"

m() {
	cd $Project
	MDIR="${DIR}/Migrations"
	rm -rf $MDIR
	cd ..
	dotnet ef migrations add SQL -n  $NAMESPACE  -o  $MDIR -c $DB_CONTEXT -p $Project
}

job() { 	
	DB_CONTEXT='JobsDbContext'
	DIR="../packages-module/Away.Jobs/DB"
	NAMESPACE="Away.Jobs.DB"

	s() {
		dotnet ef dbcontext scaffold --no-build --no-onconfiguring  ${CONN} "Pomelo.EntityFrameworkCore.MySql" \
		-c "${DB_CONTEXT}"  --context-dir "${DIR}" -o "${DIR}/Entities" -f \
		--context-namespace "${NAMESPACE}" -n "${NAMESPACE}" -p "${Project}"\
		-t "QRTZ_BLOB_TRIGGERS"\
		-t "QRTZ_CALENDARS"\
		-t "QRTZ_CRON_TRIGGERS"\
		-t "QRTZ_FIRED_TRIGGERS"\
		-t "QRTZ_JOB_DETAILS"\
		-t "QRTZ_LOCKS"\
		-t "QRTZ_PAUSED_TRIGGER_GRPS"\
		-t "QRTZ_SCHEDULER_STATE"\
		-t "QRTZ_SIMPLE_TRIGGERS"\
		-t "QRTZ_SIMPROP_TRIGGERS"\
		-t "QRTZ_TRIGGERS"
	}
	$@
}


config() { 	
	DB_CONTEXT='ConfigsDbContext'
	DIR="../packages-module/Away.Configs/DB"
	NAMESPACE="Away.Configs.DB"

	s() {	
		dotnet ef dbcontext scaffold --no-build --no-onconfiguring  ${CONN} "Pomelo.EntityFrameworkCore.MySql" \
		-c "${DB_CONTEXT}"  --context-dir "${DIR}" -o "${DIR}/Entities" -f \
		--context-namespace "${NAMESPACE}" -n "${NAMESPACE}" -p "${Project}"\
		-t "conf_configuration"\
		-t "conf_group"
	}
	$@
}


admin() { 	
	DB_CONTEXT='SystemsDbContext'
	DIR="../packages-module/Away.Admin/DB"
	NAMESPACE="Away.Admin.DB"

	s() {
		dotnet ef dbcontext scaffold --no-build --no-onconfiguring  ${CONN} "Pomelo.EntityFrameworkCore.MySql" \
		-c "${DB_CONTEXT}"  --context-dir "${DIR}" -o "${DIR}/Entities" -f \
		--context-namespace "${NAMESPACE}" -n "${NAMESPACE}" -p "${Project}"\
		-t "sys_admin"\
		-t "sys_admin_api_resource"\
		-t "sys_admin_organization"\
		-t "sys_admin_role"\
		-t "sys_admin_view_resource"\
		-t "sys_api_resource"\
		-t "sys_organization"\
		-t "sys_role"\
		-t "sys_role_api_resource"\
		-t "sys_role_view_resource"\
		-t "sys_view_resource"\
		-t "sys_view_resource_prem"
	}
	$@
}

netmap() { 	
	DB_CONTEXT='NetMapDbContext'
	DIR="../packages-module/Away.NetMap/DB"
	NAMESPACE="Away.NetMap.DB"

	s() {
		dotnet ef dbcontext scaffold --no-build --no-onconfiguring  ${CONN} "Pomelo.EntityFrameworkCore.MySql" \
		-c "${DB_CONTEXT}"  --context-dir "${DIR}" -o "${DIR}/Entities" -f \
		--context-namespace "${NAMESPACE}" -n "${NAMESPACE}" -p "${Project}"\
		-t "net_ip" \
		-t "net_port"
	}
	$@
}

$@