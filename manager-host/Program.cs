using Away.Admin;
using Away.Common.JsonConverters;
using Away.Common.Mvc.Controllers;
using Away.Common.Notify.Email;
using Away.EventBusCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Manager.Host;

public static class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        //builder.Host.AddJobs();
        //builder.Host.AddNetMap();
        //builder.Host.AddConfigs();
        builder.Host.AddSystems();
        builder.Services.AddNotifyEmail(builder.Configuration);


        builder.Services.AddAwayDI();
        builder.Services.AddMediatR()
            .AddDomainEvent(o => o.Configure(builder.Configuration))
            .AddCommand(o => o.Configure(builder.Configuration));

        builder.AddCors().AddSwagger();
        builder.AddHealthChecks().AddJwtAuth();
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddHttpClient();
        builder.Services.AddControllers().AddJsonOptions(options =>
        {
            options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
            options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        });

        builder.Services.ConfigureHttpJsonOptions(options =>
        {
            options.SerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
            options.SerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            options.SerializerOptions.Converters.Add(new DateTimeConverter());
        });

        var app = builder.Build();
        app.UseCors().UseSwagger().UseSwaggerUI();
        app
            .UseHealthChecks()
                .UseIP()
                .UseEnableBuffering()
                .UseRouting()
                .UseAuthentication()
                .UseAuthorization();
        app.MapControllers();
        app.MapMinimalApi();
        app.Run();
    }

    public static WebApplicationBuilder AddJwtAuth(this WebApplicationBuilder builder)
    {
        var jwtSettings = builder.Configuration.GetSection("JwtSettings").Get<JwtSettings>();

        builder.Services.AddAuthorization();
        builder.Services
            .AddAuthentication()
            .AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = jwtSettings!.Issuer,
                    ValidAudience = jwtSettings.Audience,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = jwtSettings.SigningKey,
                    ClockSkew = TimeSpan.Zero
                };

                o.Events = new JwtBearerEvents
                {
                    OnChallenge = context =>
                    {
                        context.HandleResponse();
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 200;

                        var result = JsonSerializer.Serialize(ApiResult.Ok(401, "未授权").Value, new JsonSerializerOptions()
                        {
                            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                            PropertyNameCaseInsensitive = true
                        });
                        context.Response.WriteAsync(result);
                        return Task.CompletedTask;
                    }
                };
            });

        return builder;
    }

    public static WebApplication UseIP(this WebApplication app)
    {
        app.UseForwardedHeaders(new ForwardedHeadersOptions
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        });
        return app;
    }

    public static WebApplication UseEnableBuffering(this WebApplication app)
    {
        app.Use((context, next) =>
        {
            context.Request.EnableBuffering();
            return next();
        });
        return app;
    }

    #region HealthChecks
    public static WebApplicationBuilder AddHealthChecks(this WebApplicationBuilder builder)
    {
        builder.Services.AddHealthChecks();
        return builder;
    }

    public static WebApplication UseHealthChecks(this WebApplication app)
    {
        app.MapHealthChecks("healthcheck");
        return app;
    }
    #endregion

    public static WebApplicationBuilder AddCors(this WebApplicationBuilder builder)
    {
        builder.Services.AddCors(o => o.AddDefaultPolicy(c => c.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod()));
        return builder;
    }

    public static WebApplicationBuilder AddSwagger(this WebApplicationBuilder builder)
    {
        builder.Services.AddSwaggerGen(o =>
        {
            //添加Jwt验证设置,添加请求头信息
            o.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new List<string>()
                    }
            });

            // 放置接口Auth授权按钮
            o.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Description = "在http header 添加参数：Authorization:Bearer <签名>",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey
            });


            // 载入xml 接口文档
            var files = Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory);
            var xmlPath = files.Where(o => o.EndsWith(".xml"));

            if (xmlPath.Any())
            {
                foreach (var path in xmlPath)
                {
                    o.IncludeXmlComments(path);
                }
            }
        });
        return builder;
    }

}