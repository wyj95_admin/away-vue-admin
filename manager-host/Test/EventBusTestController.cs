﻿using Away.EventBusCore.Commands;
using Away.EventBusCore.DomainEvents;
using Away.Manager.Common.Events.Commands;
using Away.Manager.Common.Events.DomainEvents;
using Away.Manager.Common.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Away.Manager.Host.Test;

[Route("test"), Tags("A_2"), Allow]
public class EventBusTestController : ApiController
{
    private readonly ICommandSender _sender;
    private readonly IDomainEventPublisher _publisher;
    public EventBusTestController(ICommandSender commandSender, IDomainEventPublisher domainEventPublisher)
    {
        _sender = commandSender;
        _publisher = domainEventPublisher;
    }

    [HttpGet("cmd/demo1")]
    public async Task<IActionResult> Test1(string msg)
    {
        await _sender.Send(new DemoCmd { Data = msg });
        return ApiOk();
    }

    [HttpGet("cmd/demo2")]
    public async Task<IActionResult> Test2(string msg)
    {
        var data = await _sender.Send(new Demo2Cmd { DDD = msg });
        return ApiOk(data);
    }

    [HttpGet("de/demo")]
    public async Task<IActionResult> Test3(string msg)
    {
        await _publisher.Publish(new DemoDe { Data = msg });
        return ApiOk();
    }
}
