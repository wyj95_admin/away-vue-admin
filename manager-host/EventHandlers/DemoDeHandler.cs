﻿using Away.EventBusCore.DomainEvents;
using Away.Common.Events.DomainEvents;
using System.Text.Json;

namespace Manager.Host.EventHandlers
{
    public class DemoDeHandler : IDomainEventHandler<DemoDe>
    {
        private readonly ILogger<DemoDeHandler> _logger;
        public DemoDeHandler(ILogger<DemoDeHandler> logger)
        {
            _logger = logger;
        }
        public Task Handle(DemoDe notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("{}", JsonSerializer.Serialize(notification));
            return Task.CompletedTask;
        }
    }
}
