﻿using Away.EventBusCore.Commands;
using Away.Common.Events.Commands;
using System.Text.Json;

namespace Manager.Host.EventHandlers;

public class Demo1CmdHandler : ICommandHandler<DemoCmd>
{
    private readonly ILogger<Demo1CmdHandler> _logger;
    public Demo1CmdHandler(ILogger<Demo1CmdHandler> logger)
    {
        _logger = logger;
    }

    public Task Handle(DemoCmd request, CancellationToken cancellationToken)
    {
        _logger.LogInformation("{}", JsonSerializer.Serialize(request));
        return Task.CompletedTask;
    }
}
