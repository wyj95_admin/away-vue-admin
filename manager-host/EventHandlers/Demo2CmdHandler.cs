﻿using Away.EventBusCore.Commands;
using Away.Common.Events.Commands;
using System.Text.Json;

namespace Manager.Host.EventHandlers;

public class Demo2CmdHandler : CommandHandler<Demo2Cmd, EventResult>
{
    public Demo2CmdHandler(IServiceProvider serviceProvider) : base(serviceProvider)
    {

    }

    private ILogger<Demo2CmdHandler> Logger => this.ServiceProvider.GetRequiredService<ILogger<Demo2CmdHandler>>();

    public async override Task<EventResult> Handle(Demo2Cmd request)
    {
        Logger.LogInformation("{}", JsonSerializer.Serialize(request));
        await Task.CompletedTask;
        return new EventResult { Message = request.DDD };
    }
}
