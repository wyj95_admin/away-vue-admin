# 后台管理系统
* 后端技术：.net8 EFCore minimal-api swagger doc
* 前端技术：vite  vue3   typescript
* 数据库：mysql redis rabbitMQ
---

## 系统设置
![](./images/login.jpg)
![](./images/admin.jpg)
![](./images/role.jpg)
![](./images/menu.jpg)
![](./images/dept.jpg)
![](./images/api.jpg)

## 配置中心
![](./images/config_group.jpg)
![](./images/config.jpg)

## 任务调度
![](./images/job_list.jpg)
![](./images/job_trigger.jpg)