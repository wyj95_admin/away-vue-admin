﻿namespace Away.EventBusCore.DomainEvents.Impl;

public class LocalDomainEventPublisher : IDomainEventPublisher
{
    private readonly ILogger<LocalDomainEventPublisher> _logger;
    private readonly IMediator _mediator;
    public LocalDomainEventPublisher(IMediator mediator, ILogger<LocalDomainEventPublisher> logger)
    {
        _mediator = mediator;
        _logger = logger;
    }
    public Task Listen()
    {
        return Task.CompletedTask;
    }

    public Task Publish<TEvent>(TEvent e) where TEvent : DomainEvent
    {
        return _mediator.Publish(e);
    }

    public Task Publish(string eventId, object result)
    {
        return Task.CompletedTask;
    }
}
