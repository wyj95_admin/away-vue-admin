﻿namespace Away.EventBusCore.DomainEvents;

public enum DomainEventHostType
{
    Local = 0,
    Remote = 1
}

public class DomainEventOptionsBuilder
{
    [JsonIgnore]
    public Dictionary<string, Type> EventBusTypes { get; }

    public DomainEventHostType HostType { get; set; }

    /// <summary>
    /// 主机连接
    /// </summary>
    public string? ConnectionString { get; set; }

    /// <summary>
    /// 交换机名称
    /// </summary>
    public string? ExchangeName { get; set; }

    public DomainEventOptionsBuilder()
    {
        EventBusTypes = new();
    }

    /// <summary>
    /// 读取配置
    /// </summary>
    /// <param name="configuration"></param>
    /// <remarks>
    /// <code>
    /// "DomainEventSettings": {
    ///     "HostType": "Local",
    ///     "ExchangeName": "away-event-bus",
    ///     "ConnectionString": "amqp://guest:guest@localhost"
    /// }
    /// </code>
    /// </remarks>
    /// <returns></returns>
    public void Configure(IConfiguration configuration)
    {
        var section = configuration.GetSection("DomainEventSettings");
        HostType = Enum.Parse<DomainEventHostType>(section["HostType"] ?? "Local");
        if (HostType == DomainEventHostType.Local)
        {
            return;
        }
        ConnectionString = section[nameof(ConnectionString)] ?? "amqp://guest:guest@localhost";
        ExchangeName = section[nameof(ExchangeName)] ?? "away-event-bus";
    }
}