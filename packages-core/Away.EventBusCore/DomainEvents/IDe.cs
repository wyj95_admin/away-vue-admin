﻿namespace Away.EventBusCore.DomainEvents;

public interface IDe
{
    /// <summary>
    /// 事件唯一编号
    /// </summary>
    public string EventId { get; }
}
