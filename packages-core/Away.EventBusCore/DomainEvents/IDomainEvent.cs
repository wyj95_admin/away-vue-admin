﻿namespace Away.EventBusCore.DomainEvents;
public abstract class DomainEvent : IDomainEvent
{
    public abstract string EventId { get; }
    public DateTime CreateTime => DateTime.Now;
}

public interface IDomainEvent : INotification, IDe
{

}
public interface IDomainEventHandler<in T> : INotificationHandler<T> where T : IDomainEvent { }