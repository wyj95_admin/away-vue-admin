﻿namespace Away.EventBusCore.DomainEvents;

public class DomainEventHostedService : IHostedService
{
    private readonly IServiceScope _scope;
    public DomainEventHostedService(IServiceProvider serviceProvider)
    {
        _scope = serviceProvider.CreateScope();
    }

    private IServiceProvider ServiceProvider => _scope.ServiceProvider;
    private IDomainEventHost Host => ServiceProvider.GetRequiredService<IDomainEventHost>();

    public Task StartAsync(CancellationToken cancellationToken)
    {
        return Host.Listen();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _scope.Dispose();
        return Task.CompletedTask;
    }
}
