﻿namespace Away.EventBusCore.DomainEvents;

public interface IDomainEventPublisher
{
    Task Publish<TEvent>(TEvent e) where TEvent : DomainEvent;
    Task Publish(string eventId, object result);
}

public interface IDomainEventHost
{
    Task Listen();
}