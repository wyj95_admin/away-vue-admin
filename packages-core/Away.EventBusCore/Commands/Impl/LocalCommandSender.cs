﻿namespace Away.EventBusCore.Commands.Impl;

public class LocalCommandSender : ICommandSender
{
    private readonly IMediator _mediator;
    private readonly ILogger<LocalCommandSender> _logger;
    public LocalCommandSender(IMediator mediator, ILogger<LocalCommandSender> logger)
    {
        _mediator = mediator;
        _logger = logger;
    }
    public Task Listen()
    {
        return Task.CompletedTask;
    }

    public Task Send<TEvent>(TEvent e) where TEvent : Command
    {
        return _mediator.Send(e);
    }

    public Task<TResult> Send<TResult>(Command<TResult> e)
    {
        return _mediator.Send(e);
    }

    public Task Send(string eventId, object result)
    {
        return Task.CompletedTask;
    }
}
