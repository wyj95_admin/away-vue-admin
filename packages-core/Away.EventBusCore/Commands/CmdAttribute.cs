﻿namespace Away.EventBusCore.Commands;

[AttributeUsage(AttributeTargets.Class)]
public class CmdAttribute : Attribute
{
}
