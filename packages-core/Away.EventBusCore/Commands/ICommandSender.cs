﻿namespace Away.EventBusCore.Commands;

public interface ICommandSender
{
    Task Send<TEvent>(TEvent e) where TEvent : Command;
    Task<TResult> Send<TResult>(Command<TResult> e);
    Task Send(string eventId, object result);
}

public interface ICommandHost
{
    Task Listen();
}