﻿namespace Away.EventBusCore.Commands;

public interface ICmd
{
    /// <summary>
    /// 事件唯一编号
    /// </summary>
    public string EventId { get; }
}
