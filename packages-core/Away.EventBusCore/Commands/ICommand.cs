﻿namespace Away.EventBusCore.Commands;

public abstract class Command : ICommand
{
    public abstract string EventId { get; }
}

public interface ICommand : IRequest, ICmd { }

public interface ICommandHandler<in T> : IRequestHandler<T> where T : ICommand { }