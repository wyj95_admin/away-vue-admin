﻿namespace Away.EventBusCore.Commands;

public enum CommandHostType
{
    Local = 0,
    Remote = 1
}

public class CommandOptionsBuilder
{
    [JsonIgnore]
    public Dictionary<string, Type> EventBusTypes { get; }

    public CommandHostType HostType { get; set; }
    public string? Host { get; set; }
    public string? User { get; set; }
    public string? Password { get; set; }

    public CommandOptionsBuilder()
    {
        EventBusTypes = new();
    }

    /// <summary>
    /// 读取配置
    /// </summary>
    /// <param name="configuration"></param>
    /// <remarks>
    /// <code>
    /// "CommandSettings": {
    ///     "HostType": "Local",
    ///     "Host": "localhost",
    ///     "User": "username",
    ///     "Password":"password"
    /// }
    /// </code>
    /// </remarks>
    /// <returns></returns>
    public void Configure(IConfiguration configuration)
    {
        var section = configuration.GetSection("CommandSettings");
        HostType = Enum.Parse<CommandHostType>(section["HostType"] ?? "Local");
        if (HostType == CommandHostType.Local)
        {
            return;
        }

        Host = section[nameof(Host)] ?? "localhost";
        User = section[nameof(User)];
        Password = section[nameof(Password)];
    }
}
