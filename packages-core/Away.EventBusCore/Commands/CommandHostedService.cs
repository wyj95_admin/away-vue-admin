﻿namespace Away.EventBusCore.Commands;

public class CommandHostedService : IHostedService
{
    private readonly IServiceScope _scope;
    public CommandHostedService(IServiceProvider serviceProvider)
    {
        _scope = serviceProvider.CreateScope();
    }

    private IServiceProvider ServiceProvider => _scope.ServiceProvider;
    private ICommandHost Host => ServiceProvider.GetRequiredService<ICommandHost>();

    public Task StartAsync(CancellationToken cancellationToken)
    {
        return Host.Listen();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _scope.Dispose();
        return Task.CompletedTask;
    }
}
