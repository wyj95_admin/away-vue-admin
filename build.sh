#!/bin/bash
set -e
set -x

Buid_Dir='./dist/api'
Image_Name="registry.cn-shenzhen.aliyuncs.com/1461076997/adminapi:latest"
Project="./manager-host"

pub() {
	rm -rf $Buid_Dir
	mkdir -p $Buid_Dir
	dotnet publish "${Project}" -c Release -o $Buid_Dir --nologo --no-self-contained
	rm -rf ${Buid_Dir}/*.pdb
}

img() {
	echo 'Dockerfile' >"${Buid_Dir}/.dockerignore"	
	echo '
FROM mcr.microsoft.com/dotnet/aspnet:8.0
WORKDIR /app
COPY . .
RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
ENTRYPOINT ["dotnet", "Manager.Host.dll"]

EXPOSE 80
' >"${Buid_Dir}/dockerfile"	
	cd ${Buid_Dir} && docker build -t ${Image_Name} .	
}

run() {
	pub
	img
}

push(){
	docker push ${Image_Name}
}

$@
